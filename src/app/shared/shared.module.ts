import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResizeChessboardDirective } from './directives/resize-chessboard.directive';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ResizeChessboardDirective,
    
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatMenuModule,
    FormsModule

  ],
  exports: [
    ResizeChessboardDirective,
    MatButtonModule,
    MatMenuModule,
    FormsModule
  ]
})
export class SharedModule { }
