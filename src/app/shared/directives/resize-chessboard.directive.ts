import { Directive, ElementRef, HostListener } from '@angular/core';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

/**
 * @directive 'resize' allows chessboard resizing while holding the
 * expand button on the bottom right
 */

@Directive({
  selector: '[resize]',
})
export class ResizeChessboardDirective {
  chessboard!: HTMLElement;
  button!: HTMLElement;
  window!: any;
  piece!: HTMLElement;
  chessboardContainer!: HTMLElement;

  constructor(
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: Document
  ) {}

  originalStyle!: String;

  ngOnInit() {
    this.window = this.document.defaultView;
    this.button = this.elementRef.nativeElement;
    this.originalStyle = this.button.style.cssText;

    //reaching the container parent
    this.chessboard =
      this.elementRef.nativeElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;

    // this.piece =

    this.chessboardContainer =
      this.elementRef.nativeElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;

  }

  $scope: any = this;

  @HostListener('mousedown', ['$event'])
  resizeOnMouseDown(event: MouseEvent) {
    this.window.addEventListener('mousemove', this.resize);

    this.button.style.background = 'rgb(188, 228, 188)';
    this.button.style.borderRadius = '8px';
    this.button.style.cursor = 'nwse-resize';

    this.window.addEventListener('mouseup', () => {
      this.window.removeEventListener('mousemove', this.resize);
      this.button.style.background = 'transparent';
    });
  }

  clientY!: number;
  clientX!: number;

  clientYBhs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  clientXBhs: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  scaleValue: number = 1;

  //arrow function preserve the this scope
  resize = (e: MouseEvent) => {
    
    this.clientX = e.clientX;
    this.clientY = e.clientY;
    
    this.scaleValue = 0.0005 * (this.clientX + this.clientY) + 0.4;

    if(this.scaleValue < 0.5 || this.scaleValue > 1.4){ return };
    
    this.chessboardContainer.style.transform = `scale(${ this.scaleValue})`;
  };
}
