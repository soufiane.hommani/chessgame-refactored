import { NgModule, isDevMode } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ChessgameModule } from './chessgame/chessgame.module';
import { FormsModule } from '@angular/forms';
import { PossibleMovesService } from './chessgame/services/possible-moves.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { chessboardReducer } from './chessgame/interfaces/store/slices/chessboard-slice/reducer';
import { possibleMovesReducer } from './chessgame/interfaces/store/slices/possible-moves-slice/reducer';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    AppRoutingModule,
    ChessgameModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
    EffectsModule.forRoot([]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
