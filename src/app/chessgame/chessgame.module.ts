import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChessboardContainerComponent } from './components/chessboard-container/chessboard-container.component';
import { ChessgameRoutingModule } from './chessgame-routing.module';
import { ChessboardComponent } from './components/chessboard/chessboard.component';
import { SquareComponent } from './components/square/square.component';
import { SharedModule } from '../shared/shared.module';
import { PiecesZIndexComponent } from './components/pieces-z-index/pieces-z-index.component';
import { FormsModule } from '@angular/forms';
import { PawnService } from './services/pieces/pawn.service';
import { PossibleMovesService } from './services/possible-moves.service';
import { chessboardReducer } from './interfaces/store/slices/chessboard-slice/reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PossibleMovesEffects } from './interfaces/store/slices/possible-moves-slice/effect';
import { possibleMovesReducer } from './interfaces/store/slices/possible-moves-slice/reducer';
import { turnToPlayReducer } from './interfaces/store/slices/turn-to-play-slice/reducer';
import { lastPieceTakenReducer } from './interfaces/store/slices/last-piece-taken-slice/reducer';
import { kingReducer } from './interfaces/store/slices/king-slice/reducer';
import { ChessboardEffects } from './interfaces/store/slices/chessboard-slice/effect';
import { KingEffects } from './interfaces/store/slices/king-slice/effect';
import { CoordsUtilService } from './services/coords-util.service';
import { KingManagementService } from './services/king-management.service';

@NgModule({
  declarations: [
    ChessboardContainerComponent,
    ChessboardComponent,
    SquareComponent,
    PiecesZIndexComponent,
  ],
  imports: [
     CommonModule,
     ChessgameRoutingModule,
     SharedModule,
     StoreModule.forFeature('chessboard', chessboardReducer),
     StoreModule.forFeature('turnToPlay', turnToPlayReducer),
     StoreModule.forFeature('possibleMoves', possibleMovesReducer),
     StoreModule.forFeature('lastPieceTaken', lastPieceTakenReducer), 
     StoreModule.forFeature('kingStatus', kingReducer), 
     

     EffectsModule.forFeature([PossibleMovesEffects, ChessboardEffects, KingEffects])

    ],
  exports: [ChessboardContainerComponent],
  providers: [CoordsUtilService, KingManagementService]
  // providers: [PossibleMovesService],
})
export class ChessgameModule {}
