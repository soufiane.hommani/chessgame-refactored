import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChessboardContainerComponent } from './components/chessboard-container/chessboard-container.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  // {
  //   path: '',
  //   pathMatch: 'full',
  //   redirectTo: 'home'
  // },


  {
    path: '',
    component: ChessboardContainerComponent
  }



];



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ChessgameRoutingModule { }
