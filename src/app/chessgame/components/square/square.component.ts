import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import {
  ChessboardSettingsService,
  chessBoardTheme,
} from 'src/app/core/services/chessboard-settings.service';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Coords } from 'src/app/core/interfaces/coords';
import {
  selectAfterMovePosition,
  selectBeforeMovePosition,
} from '../../interfaces/store/slices/possible-moves-slice/selector';
import { Piece } from 'src/app/core/interfaces/piece';
import { selectCheckedKing } from '../../interfaces/store/slices/king-slice/selector';

@Component({
  selector: 'square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss'],
})
export class SquareComponent implements OnInit {
  @Input() black: boolean = false;
  @Input() prenable: boolean = false;
  @Input() isBeforeOrLastMove: boolean = false;
  @Input() isChecked: boolean = false;
  @Input() isClicked: boolean = false;

  currentTheme!: chessBoardTheme;

  constructor(private chessboardSettings: ChessboardSettingsService, 
    private store: Store<ChessgameState>
    ) {}

  ngOnInit(): void {
    this.chessboardSettings.currentTheme$.subscribe((t: chessBoardTheme) => {
      this.currentTheme = t;
    });

   
  }

  ngOnChanges(changes: SimpleChanges): void {}

  

  getStyle() {

    if(this.isClicked){
      return this.black
      ? { backgroundColor: this.currentTheme.current, color: 'white' }
      : { backgroundColor: this.currentTheme.current, color: 'black' };
    }


    if (!this.isBeforeOrLastMove) {
      return this.black
        ? { backgroundColor: this.currentTheme.white, color: 'white' }
        : { backgroundColor: this.currentTheme.black, color: 'black' };
    }

    return this.black
      ? {
          backgroundColor: this.currentTheme.white,
          color: 'white',
          backgroundImage: this.currentTheme.beforeAfterPosition,
        }
      : {
          backgroundColor: this.currentTheme.black,
          color: 'black',
          backgroundImage: this.currentTheme.beforeAfterPosition,
        };
  }
}
