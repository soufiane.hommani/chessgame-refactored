import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Coords } from 'src/app/core/interfaces/coords';
import { MOCK_PIECE, Piece } from 'src/app/core/interfaces/piece';
import { ChessboardSettingsService } from 'src/app/core/services/chessboard-settings.service';
import { PossibleMovesService } from '../../services/possible-moves.service';
import { Observable } from 'rxjs';
import { selectChessboard } from '../../interfaces/store/slices/chessboard-slice/selector';
import { Store, select } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { addThreateningMovesToPossibleEnnemyMoves, emptyPossibleMoves, loadAllPossibleEnnemyMoves, loadPossibleMoves, movePiece, setCurrentPieceClicked, setDefendingPossibleMoves } from '../../interfaces/store/slices/possible-moves-slice/action';
import {
  selectCommonCoveringCoords,
  selectLastPieceClicked,
  selectPossibleMoves,
} from '../../interfaces/store/slices/possible-moves-slice/selector';
import { getTurnToPlay } from '../../interfaces/store/slices/lastpiece-clicked-slice/selectors';
import { changeTurnToPlay } from '../../interfaces/store/slices/turn-to-play-slice/action';
import { setLastPieceTaken } from '../../interfaces/store/slices/last-piece-taken-slice/action';
import { selectLastPieceTaken } from '../../interfaces/store/slices/last-piece-taken-slice/selector';
import { isKingChecked, loadPossibleKingCheckedMove, removeThreateningPiece } from '../../interfaces/store/slices/king-slice/action';
import { selectCheckedKing, selectEmptyCoordsKingVSThreat, selectFirstThreateningPiece, selectThreateningPieces } from '../../interfaces/store/slices/king-slice/selector';
import { PieceService } from '../../services/piece.service';
import { KingManagementService } from '../../services/king-management.service';
import { KingService } from '../../services/pieces/king.service';
import { CoordsUtilService } from '../../services/coords-util.service';
import { animate, style, transition, trigger } from '@angular/animations';
import { updateGrandRook, updateSmallRook } from '../../interfaces/store/slices/chessboard-slice/action';

@Component({
  selector: 'app-chessboard-container',
  templateUrl: './chessboard-container.component.html',
  styleUrls: ['./chessboard-container.component.scss'],
})
export class ChessboardContainerComponent implements OnInit {
  mockOrientation: boolean = true;
  chessboardOrientation!: boolean;
  propUsername: string = '';
  /**
   * @property pieces allows us to iterate on the 64 cases of a chessboard
   *
   */

  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  possibleMoves$: Observable<Array<Coords>> = this.store.pipe(
    select(selectPossibleMoves)
  );
  lastPieceClicked$: Observable<Piece | null> = this.store.pipe(
    select(selectLastPieceClicked)
  );
  lastPieceTaken$: Observable<Piece | null> = this.store.pipe(
    select(selectLastPieceTaken)
  );
  commonCoveringCoords$: Observable<Coords[] | null> = this.store.pipe(
    select(selectCommonCoveringCoords)
  );
  turnToPlay$: Observable<string> = this.store.pipe(
    select(getTurnToPlay)
  );
  checkedKing$: Observable<Piece> = this.store.pipe(
    select(selectCheckedKing)
  );
  emptyCoordsKingVSThreat$: Observable<Coords[]> = this.store.pipe(
    select(selectEmptyCoordsKingVSThreat)
  );

  firstThreateningPiece$: Observable<Piece | null> = this.store.pipe(
    select(selectFirstThreateningPiece)
  );

  ThreateningPieces$: Observable<Piece[]> = this.store.pipe(
    select(selectThreateningPieces)
  );




  pieces: Array<Piece> = [];
  possibleMoves: Coords[] = [];
  lastPieceClicked!: Piece;
  lastPieceTaken!: Piece;
  turnToPlay!: string;
  checkedKing!: Piece;
  emptyCoordsKingVSThreat: Coords[] = [];
  threateningPiece?: Piece | null;
  threateningPieces: Piece[] = [];
  commonCoveringCoords: Coords[] = [];

  constructor(
    private chessboardSettings: ChessboardSettingsService,
    private store: Store<ChessgameState>,
    private pbm: PossibleMovesService,
    private possibleMovesService: PossibleMovesService,
    private pieceService: PieceService,
    private kingManagementService: KingManagementService,
    private kingService: KingService,
    private coordsService: CoordsUtilService
  ) {


  }

  ngOnInit(): void {


    this.commonCoveringCoords$.subscribe((cc: Coords[] | null) => {
      this.commonCoveringCoords = cc as Coords[]
    });
    this.possibleMoves$.subscribe((c: Coords[]) => (this.possibleMoves = c));
    this.lastPieceClicked$.subscribe((l: Piece | null) => this.lastPieceClicked = l as Piece);


    this.lastPieceTaken$.subscribe((l: Piece | null) => {

      this.lastPieceTaken = <Piece>l;
      const threateningPieceTaken = this.threateningPieces.filter(p => this.pieceService.isSamePiece(l!, p))[0];

      if (!threateningPieceTaken) {
        return;
      }

      this.store.dispatch(removeThreateningPiece({ threateningPiece: threateningPieceTaken }))
    });

    this.turnToPlay$.subscribe((t: string) => this.turnToPlay = t);
    this.checkedKing$.subscribe((k: Piece) => this.checkedKing = k);
    this.emptyCoordsKingVSThreat$.subscribe((empty: Coords[]) => this.emptyCoordsKingVSThreat = empty);
    this.firstThreateningPiece$.subscribe((thr: Piece | null) => this.threateningPiece = thr);
    this.firstThreateningPiece$.subscribe((thr: Piece | null) => this.threateningPiece = thr);
    this.ThreateningPieces$.subscribe((thrs: Piece[]) => this.threateningPieces = thrs);
  }
  //change chessboard settings methods:
  changeCbOrientation() {
    this.chessboardSettings.setCbOrientation(this.mockOrientation);
    this.mockOrientation = !this.mockOrientation;
    // this.styleArray.reverse();
  }
  changeChessboardTheme(theme: string) {
    this.chessboardSettings.changeChessboardTheme(theme);
  }
  position(coords: Coords, piece: Piece): string {
    if (coords) {
      return this.chessboardSettings.coordsToCssTopAndLeftPositions(
        coords,
        this.mockOrientation
      );
    }
    return '';
  }

  isTurnToPlay(turnToPlay: string, pieceName: string) {
    return turnToPlay === pieceName;
  }

  jumpState: any;

  onClick(event: MouseEvent): void {
    const pieceElement = event.target as HTMLElement;
    this.jumpState = { value: pieceElement };
  }

  handleClick(piece: Piece, index: number) {

    const {pieceName, color, coords} = piece;

    const possibleMoves: Coords[] = this.possibleMovesService.showPossibleMovesNotObservable(piece)
     //console.log('possibleMoves pice cliquée', possibleMoves)
    const isATargetCoords = this.possibleMovesService.isPossibleMove(piece, this.possibleMoves);

    if (piece.pieceName !== 'empty' && this.kingManagementService.isCoveringKing(piece, possibleMoves) && !isATargetCoords
      && this.commonCoveringCoords.length === 0) {
      //console.log('atteri la 1')
      return;
    }

    //towers previously css style rookable should be reset
    this.store.dispatch(updateSmallRook({smallRookStatus: false, rookableTower: null}));
    this.store.dispatch(updateGrandRook({grandRookStatus: false, rookableTower: null}));


    
    //console.log('this.commonCoveringCoords', this.commonCoveringCoords)
    this.store.dispatch(loadAllPossibleEnnemyMoves({ turnToPlay: this.turnToPlay, piece: piece }));
    let emptyPossibleMovesArray = false
    const isTurnToPlay = this.isTurnToPlay(this.turnToPlay, <string>piece.color)
    const isPieceNotTaken = this.lastPieceTaken ? PossibleMovesService.isSameCoords(this.lastPieceTaken.coords, piece.coords) : false;
    
    this.store.dispatch(setCurrentPieceClicked({piece}));
    //décommenter pour remettre les tours
    /* if(!isTurnToPlay && !isATargetCoords ){
      this.store.dispatch(emptyPossibleMoves());
      return;
    }
 */
    if (isATargetCoords) {
      this.store.dispatch(setCurrentPieceClicked({piece: MOCK_PIECE}));
      this.store.dispatch(emptyPossibleMoves());
      this.store.dispatch(changeTurnToPlay({ color: this.lastPieceClicked.color == 'white' ? 'black' : 'white' }));
      this.store.dispatch(movePiece({ piece: piece, lastPieceClicked: this.lastPieceClicked }));
      this.store.dispatch(isKingChecked({ pieceMoved: this.lastPieceClicked }));
      //il faut empty les possible coords 
      if (piece.pieceName !== 'empty') {
        emptyPossibleMovesArray = true
        this.store.dispatch(setLastPieceTaken({ lastPieceTaken: piece }))
      }
    }
    if (this.checkedKing && this.turnToPlay == this.checkedKing.color
      && piece.color === this.checkedKing.color && piece.pieceName != 'king') {
        //console.log('atteri la 3')
      //this.store.dispatch(loadPossibleKingCheckedMove({king: this.checkedKing, pieceCliecked: piece}))
      const pieceMove: Coords[] = this.possibleMovesService.showPossibleMovesNotObservable(piece);

      //adding the threat piece coords to emptyCoordsKingVSThreat 
      //because a piece can defend by taking the threatening piece
      //if two threats in the same alignment and piece move is king it cant be added
      if (this.pieceService.isSamePiece(piece, this.checkedKing) && this.threateningPieces.length > 1) {
        //console.log('atteri la 4')
        return;
      }

      
      this.emptyCoordsKingVSThreat = this.threateningPiece!.coords ? [...this.emptyCoordsKingVSThreat, this.threateningPiece!.coords!] : this.emptyCoordsKingVSThreat;
      const commonCoords = this.possibleMovesService.getCommonCoords(pieceMove, this.emptyCoordsKingVSThreat);


      if (commonCoords.length) {

        this.store.dispatch(setDefendingPossibleMoves({ defendingCoords: commonCoords, defendingPiece: piece }));
      }

      return;

    }

    if (!emptyPossibleMovesArray) {
      //console.log('atteri la 6')

      if(this.threateningPieces.length !== 0 && this.checkedKing && this.pieceService.isSameCoords(piece.coords, this.checkedKing.coords)){

        this.kingService.evaluateCheckedConfiguration();
        const firstThreat = this.threateningPieces[0];
        const firstThreatPossibleAbsoluteMoves: Coords[] =  this.possibleMovesService
        .showPossibleMovesAbsoluteNotObservable(firstThreat);

        const isFirsThreatCovered = this.pieceService.isFirsThreatCovered(firstThreat);

        //console.log('this.pieceService.isFirsThreatCovered(firstThreat);', isFirsThreatCovered)

/*        8  . . . . . . . .
          7  . . . . . . . .
          6  . . . . . . . .
          5  . . . X X . . . 
          4  . . bq wK X X X
          3  . . . X X . . .
          2  . . . . . . . .
          1  . . . . . . . .
             a b c d e f g h
          X: interdit
*/
        this.store.dispatch(addThreateningMovesToPossibleEnnemyMoves({threateningMove: firstThreatPossibleAbsoluteMoves}))
        //console.log('[this.threateningPieces]', this.threateningPieces)
      }
      this.store.dispatch(loadPossibleMoves({ piece }));
    }


    // const possibleMovesToPiece = this.coordsService.coordsToPiecesVariant(this.possibleMoves);
    // console.log('king possibleMovesToPiece', possibleMovesToPiece)
    
  }
}
