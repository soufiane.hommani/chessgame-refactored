import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PiecesZIndexComponent } from './pieces-z-index.component';

describe('PiecesZIndexComponent', () => {
  let component: PiecesZIndexComponent;
  let fixture: ComponentFixture<PiecesZIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PiecesZIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PiecesZIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
