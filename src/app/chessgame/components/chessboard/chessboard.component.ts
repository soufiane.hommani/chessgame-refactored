import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Coords } from 'src/app/core/interfaces/coords';
import { ChessboardSettingsService } from 'src/app/core/services/chessboard-settings.service';
import { PossibleMovesService } from '../../services/possible-moves.service';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { Store, select } from '@ngrx/store';
import { selectAfterMovePosition, selectBeforeMovePosition, selectCurrentPieceClicked, selectLastPieceClicked } from '../../interfaces/store/slices/possible-moves-slice/selector';
import { Observable } from 'rxjs';
import { Piece } from 'src/app/core/interfaces/piece';
import { selectCheckedKing } from '../../interfaces/store/slices/king-slice/selector';
import { CoordsUtilService } from '../../services/coords-util.service';
import { selectGrandRook, selectSmallRook } from '../../interfaces/store/slices/chessboard-slice/selector';

@Component({
  selector: 'chessboard',
  templateUrl: './chessboard.component.html',
  styleUrls: ['./chessboard.component.scss'],
})
export class ChessboardComponent implements OnInit {

  /**
   * @params sixtyFour : array of 64 cases we iterate on
   */
  sixtyFour: Array<number> = new Array(64).fill(0).map((_, i) => i);

  /**
   * @params ordinate: value of the Y axis case for notation
   */
  ordinate!: number;
  absciss!: string;
  letters: Array<string> = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

  @Input() chessboardOrientation!: boolean;
  @Input() possibleCoords!: Array<Coords>;

  kingCheckedState$: Observable<Piece | null> = this.store.pipe(
    select(selectCheckedKing)
  );
  checkedKing!: Piece;


  beforeMovePosition$: Observable<Coords | null> = this.store.pipe(
    select(selectBeforeMovePosition)
  );
  beforeMovePosition!: Coords;

  currentPieceClicked$: Observable<Piece | null> = this.store.pipe(
    select(selectCurrentPieceClicked)
  );
  currentPieceClicked!: Piece;

  smallRook$: Observable<{ status: boolean; rookableTower: Piece | null }> = this.store.pipe(
    select(selectSmallRook)
  );
  smallRook!: { status: boolean; rookableTower: Piece | null };


  grandRook$: Observable<{ status: boolean; rookableTower: Piece | null }> = this.store.pipe(
    select(selectGrandRook)
  );
  grandRook!: { status: boolean; rookableTower: Piece | null };


  constructor(private chessboardSettings: ChessboardSettingsService,
    private store: Store<ChessgameState>, private pbm: PossibleMovesService,
    private coordUtilService: CoordsUtilService) { }

  ngOnInit(): void {
    this.chessboardOrientation = this.chessboardSettings.getCbOrientation();

    this.kingCheckedState$.subscribe((k: Piece | null) => {
      this.checkedKing = k as Piece;
    });

    this.beforeMovePosition$.subscribe((bP: Coords | null) => {
      this.beforeMovePosition = bP as Coords;
    });


    this.currentPieceClicked$.subscribe((sP: Piece | null) => {
      this.currentPieceClicked = sP as Piece;
    });


    this.smallRook$.subscribe((sR: { status: boolean; rookableTower: Piece | null }) => {
      this.smallRook = { status: sR.status,  rookableTower: sR.rookableTower };
    });

    this.grandRook$.subscribe((sR: { status: boolean; rookableTower: Piece | null }) => {
      this.grandRook = { status: sR.status,  rookableTower: sR.rookableTower };
    });

  }

  isClicked(pos: Coords): boolean {


    if (!this.currentPieceClicked || this.currentPieceClicked.pieceName === 'empty') {
      return false;
    }
    return PossibleMovesService.isSameCoords(pos, this.currentPieceClicked.coords)
  }

  isChecked(pos: Coords): boolean {
    if (!this.checkedKing) {
      return false;
    }

    return PossibleMovesService.isSameCoords(pos, this.checkedKing.coords);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.chessboardOrientation && changes.chessboardOrientation.currentValue !== changes.chessboardOrientation.previousValue
      && !changes.chessboardOrientation.firstChange) {
      this.sixtyFour = this.sixtyFour.reverse();
    }
  }

  /**
   * @method xy transform an index of the chessboard array (@see sixtyFour) into a coordinate object
   * @param i
   * @returns Coord
   */
  xy(i: number): Coords {
    return { x: i % 8, y: Math.floor(i / 8) };
  }

  /**
   * @method isBlack set input black to true or false allowing a square component to be black or white
   * @see /chessboard.component.html
   * @param pos
   * @returns boolean
   */
  isBlack(pos: Coords): boolean {
    return (pos.x + pos.y) % 2 === 1;
  }

  /**
   *
   * @method isANumberCoordinate determine wether pos a number notation or not
   * and set the value of it (this.ordinate)
   */
  isANumberCoordinate(pos: Coords): boolean {
    this.ordinate = this.chessboardOrientation ? 7 - pos.y + 1 : pos.y + 1;
    return pos.x === 7 ? true : false;
  }

  /**
   *
   * @method isANumberCoordinate determine wether pos a number notation or not
   * and set the value of it (this.ordinate)
   */
  isALetterCoordinate(pos: Coords): boolean {
    this.absciss = this.chessboardOrientation
      ? this.letters[pos.x]
      : this.letters[7 - pos.x];
    return pos.y === 7 ? true : false;
  }

  isResizeButton(pos: Coords) {
    return this.chessboardOrientation ? pos.y === 7 && pos.x === 7 ? true : false :
      pos.y === 0 && pos.x === 0 ? true : false;
  }

  isBeforeOrLastMove(pos: Coords): boolean {
    return this.pbm.isBeforeOrLastMove(pos);
  }

  isAPossibleMove(pos: Coords): boolean {
    return this.possibleCoords.some(coords => coords.x === pos.x && coords.y === pos.y);
  }

  isAPiece(pos: Coords): boolean {
    return this.pbm.isAPiece(pos);
  }

  isRookable(pos: Coords): boolean {

    const posToPiece: Piece[] = this.coordUtilService.coordsToPiecesVariant([pos]) as Piece[];

    if(posToPiece[0] && posToPiece[0].pieceName === 'tower' &&
     this.smallRook &&
      this.coordUtilService.isSameCoords(pos, this.smallRook.rookableTower?.coords as Coords)
      && this.currentPieceClicked.pieceName === 'king'){
      return this.smallRook.status;
    }

    if(posToPiece[0] && posToPiece[0].pieceName === 'tower' &&
    this.grandRook &&
     this.coordUtilService.isSameCoords(pos, this.grandRook.rookableTower?.coords as Coords)
     && this.currentPieceClicked.pieceName === 'king'){
     return this.grandRook.status;
   }

    //smallRook & grandRook boolean Slices in store
   return false
  }


}
