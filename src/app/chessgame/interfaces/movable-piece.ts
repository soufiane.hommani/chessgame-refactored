import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';

export interface MovablePiece {
  /**
   * @method showPossibleAbsoluteCoords returns possible coords
   * for a given clicked piece and chessboard orientation
   *  without taking king state into considerations
   */
  showPossibleAbsoluteCoords(piece: Piece): Array<Coords>;
}
