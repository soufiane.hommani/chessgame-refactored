import { Injectable, OnInit } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, tap } from 'rxjs/operators';
import { emptyThreateningPieces, isKingChecked, kingIsChecked, kingIsUnChecked, loadPossibleKingCheckedMove, removeThreateningPiece, updateFirstThreat, updateThreatWithItsIndex } from '../king-slice/action';
import { KingService } from 'src/app/chessgame/services/pieces/king.service';
import { Piece } from 'src/app/core/interfaces/piece';
import { Store, select } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/chessgame.state';
import { Observable } from 'rxjs';
import { selectCheckedKing, selectThreateningPieces } from './selector';
import { KingStatusState } from '../../interfaces/king-status.state';
import { PieceService } from 'src/app/chessgame/services/piece.service';
@Injectable()
export class KingEffects {

  kingCheckedState$: Observable<Piece | null> = this.store.pipe(
    select(selectCheckedKing)
  );

  ThreateningPieces$: Observable<Piece[]> = this.store.pipe(
    select(selectThreateningPieces)
  );

  checkedKing!: Piece;
  threateningPieces: Piece[] = [];

  constructor(
    private actions$: Actions,
    private kingService: KingService,
    private store: Store<ChessgameState>,
    private pieceService: PieceService
  ) {

    this.kingCheckedState$.subscribe((k: Piece | null) => {
      this.checkedKing = k as Piece;
    });

    this.ThreateningPieces$.subscribe((t: Piece[] | null ) => {
      this.threateningPieces = t as Piece[];
    });

  }

  isKingChecked$ = createEffect(() =>
    this.actions$.pipe(
      ofType(isKingChecked),
      map((data: any) => {
        let nextKing = this.kingService.findNextPlayerKing(data.pieceMoved);
        let isKingChecked = this.kingService.isKingChecked(nextKing!); // Trigger the service method

        if (isKingChecked) {
          return kingIsChecked({ kingChecked: <Piece>nextKing });
        } else {
          return kingIsUnChecked({ kingChecked: null });
        }
      })
    )
  );


  loadKingPossibleCheckedMoves$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadPossibleKingCheckedMove),  // Listening for the action
      tap(({ king }) => {  // Extracting the king property from the action's payload
        //console.log("king is cheked in the effect", king);
      })
    ), { dispatch: false } // No action is dispatched in response, hence dispatch is set to false
  );

  updateFirstThreat$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateFirstThreat),
      map(action => {
        const updatedPiece = action.threateningPiece;
        // Find and replace the piece in the threateningPieces array
        const updateThreatIndex = this.threateningPieces.findIndex(piece => this.pieceService.isSamePiece(piece, updatedPiece))
        //console.log('updatedThreateningPiece', updatedPiece)
        return updateThreatWithItsIndex({ threateningPieceIndex: updateThreatIndex, updatedThreateningPiece: updatedPiece });
      })
    )
  );


  // Add this effect in the KingEffects class
  emptyThreateningPieces$ = createEffect(() =>
    this.actions$.pipe(
      ofType(emptyThreateningPieces),
      tap(() => {
        // This effect is only triggering a state change, no need for further actions
      })
    ), { dispatch: false } // No need to dispatch an action after this effect
  );


}
