import { createAction, props } from '@ngrx/store';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';

export const isKingChecked = createAction(
  '[King] is King Checked',
  props<{pieceMoved: Piece}>()
);

export const storeEmptyCoordsBetweenKingAndThreat = createAction(
  '[King] empty coords between king and threat',
  props<{empty: Coords[]}>()
);

export const kingIsChecked = createAction(
  '[King] King is Checked',
  props<{kingChecked: Piece}>()
);


export const kingIsUnChecked = createAction(
  '[King] King is unChecked',
  props<{kingChecked: Piece | null}>()
);

export const kingCheckmate = createAction(
  '[King] King Checkmate',
  props<{ isCheckmate: boolean }>()
);

export const loadPossibleKingCheckedMove = createAction(
  '[King] Load Possible King Checked Move',
  props<{ king: Piece, pieceCliecked: Piece }>()
);

export const loadPossibleKingCheckedMoveSuccess = createAction(
  '[King] Load Possible King Checked Move Success',
  props<{ moves: Coords[] }>()
);

export const loadPossibleKingCheckedMoveFailure = createAction(
  '[King] Load Possible King Checked Move Failure',
  props<{ error: string }>()
);

export const storeThreateningPieces = createAction(
  '[King] store threatening pieces',
  props<{ threateningPieces: Piece[] }>()
);

export const emptyThreateningPieces = createAction(
  '[King] empty threatening pieces',
  props<any>()
);



export const removeThreateningPiece = createAction(
  '[King] remove threatening pieces',
  props<{ threateningPiece: Piece }>()
);


export const updateFirstThreat = createAction(
  '[King] find first threat index',
  props<{ threateningPiece: Piece }>()
);


export const updateThreatWithItsIndex = createAction(
  '[King] update first threat index',
  props<{ threateningPieceIndex: number, updatedThreateningPiece: Piece }>()
);




