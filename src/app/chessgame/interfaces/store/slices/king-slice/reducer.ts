import { createReducer, on } from '@ngrx/store';
import * as KingActions from './action';
import { KingStatusState } from '../../interfaces/king-status.state';
import { KingsState } from '../../interfaces/kings.state';

const initialKingState: KingsState = {
  isCheck: false,
  isCheckMate: false,
  kingCheckPossibleMove: [],
};

const initialKingsStatusState: KingStatusState = {
  blackKing: initialKingState,
  whiteKing: initialKingState,
  kingChecked: null,
  threateningPieces: [],
  emptyCoordsKingVSThreat: []
};

export const kingReducer = createReducer(
  initialKingsStatusState,

  on(KingActions.storeThreateningPieces, (state, { threateningPieces: pieces }) => ({
    ...state,
    threateningPieces: pieces,
  })),

  on(KingActions.loadPossibleKingCheckedMove, (state, { king }) => ({
    ...state,
    king: king,
    kingCheckPossibleMove: [],
  })),
  on(KingActions.loadPossibleKingCheckedMoveSuccess, (state, { moves }) => ({
    ...state,
    kingCheckPossibleMove: moves,
  })),
  on(KingActions.loadPossibleKingCheckedMoveFailure, (state, { error }) => ({
    ...state,
    kingCheckPossibleMove: [],
    // Handle the error as needed
  })),
  on(KingActions.kingIsChecked, (state, { kingChecked }) => ({
    ...state,
    kingChecked: kingChecked,
  })),
  on(KingActions.storeEmptyCoordsBetweenKingAndThreat, (state, { empty }) => ({
    ...state,
    emptyCoordsKingVSThreat: empty,
  })),
  on(KingActions.kingIsUnChecked, (state, { kingChecked }) => ({
    ...state,
    kingChecked: kingChecked,
  })),
  on(KingActions.removeThreateningPiece, (state, { threateningPiece }) => ({
    ...state,
    threateningPieces: state.threateningPieces.filter(piece => piece.pieceName !== threateningPiece.pieceName || piece.coords.x !== threateningPiece.coords.x || piece.coords.y !== threateningPiece.coords.y),
  })),

  on(KingActions.updateThreatWithItsIndex, (state, { threateningPieceIndex, updatedThreateningPiece }) => {
    if (threateningPieceIndex >= 0) {
      // Clone the array to avoid direct state mutation
      const updatedThreateningPieces = [...state.threateningPieces];
      updatedThreateningPieces[threateningPieceIndex] = updatedThreateningPiece;
      return {
        ...state,
        threateningPieces: updatedThreateningPieces,
      };
    }
    return state;
  }),
  on(KingActions.emptyThreateningPieces, state => ({
    ...state,
    threateningPieces: [], // Empty the array of threatening pieces
  })),
);