import { createFeatureSelector, createSelector } from '@ngrx/store';
import { KingStatusState } from '../../interfaces/king-status.state';
import { KingsState } from '../../interfaces/kings.state';


// Select the KingStatusState slice
export const selectKingStatusState = createFeatureSelector<KingStatusState>('kingStatus');

// Select the blackKing from the KingStatusState
export const selectBlackKing = createSelector(
  selectKingStatusState,
  (state: KingStatusState) => state.blackKing
);

export const selectCheckedKing = createSelector(
  selectKingStatusState,
  (state: KingStatusState) => state?.kingChecked!
);

// Select the whiteKing from the KingStatusState
export const selectWhiteKing = createSelector(
  selectKingStatusState,
  (state: KingStatusState) => state.whiteKing
);

// Select the isCheck property of the blackKing
export const selectBlackKingIsCheck = createSelector(
  selectBlackKing,
  (blackKing: KingsState) => blackKing.isCheck
);

// Select the isCheck property of the whiteKing
export const selectWhiteKingIsCheck = createSelector(
  selectWhiteKing,
  (whiteKing: KingsState) => whiteKing.isCheck
);

// Select the isCheckMate property of the blackKing
export const selectBlackKingIsCheckMate = createSelector(
  selectBlackKing,
  (blackKing: KingsState) => blackKing.isCheckMate
);




// Select the isCheckMate property of the whiteKing
export const selectWhiteKingIsCheckMate = createSelector(
  selectWhiteKing,
  (whiteKing: KingsState) => whiteKing.isCheckMate
);

// Select the kingCheckPossibleMove property of the blackKing
export const selectBlackKingCheckPossibleMove = createSelector(
  selectBlackKing,
  (blackKing: KingsState) => blackKing.kingCheckPossibleMove
);

// Select the kingCheckPossibleMove property of the whiteKing
export const selectWhiteKingCheckPossibleMove = createSelector(
  selectWhiteKing,
  (whiteKing: KingsState) => whiteKing.kingCheckPossibleMove
);


// Select the first threatening piece from the threateningPieces array
export const selectFirstThreateningPiece = createSelector(
  selectKingStatusState,
  (state: KingStatusState) => state.threateningPieces[0]
);

export const selectThreateningPieces = createSelector(
  selectKingStatusState,
  (state: KingStatusState) => state.threateningPieces
);


// Select the first threatening piece from the threateningPieces array
export const selectEmptyCoordsKingVSThreat = createSelector(
  selectKingStatusState,
  (state: KingStatusState) => state.emptyCoordsKingVSThreat
);
