import { createAction, props } from '@ngrx/store';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';

export const loadPossibleMoves = createAction(
  '[possibleMoves] Load Possible Moves',
  props<{ piece: Piece }>()
);


export const setCurrentPieceClicked = createAction(
  '[possibleMoves] set current Piece Clicked',
  props<{ piece: Piece }>()
);


export const loadPossibleMovesSuccess = createAction(
  '[possibleMoves] Load Possible Moves Success',
  props<{ possibleMoves: Array<Coords>; lastPieceClicked: Piece }>()
);

export const loadPossibleMovesFailure = createAction(
  '[possibleMoves] Load Possible Moves Failure',
  props<{ error: string }>()
);

export const loadAllPossibleMoves = createAction(
  '[possibleMoves] Load All Possible Moves',
  props<{ allPossibleMoves: Array<Coords> }>()
);


export const movePiece = createAction(
  '[possibleMoves] Move Piece',
  props<{ piece: Piece; lastPieceClicked: Piece | null}>()
);

export const movePieceSuccess = createAction(
  '[possibleMoves] Move Piece Success',
  props<{ piece: Piece; lastPieceClicked: Piece | null; newCoords: Coords }>()
);

export const movePieceFailure = createAction(
  '[possibleMoves] Move Piece Failure',
  props<{ error: string }>()
);

export const smallRook = createAction(
  '[possibleMoves] small Rook',
  props<{ tower: Piece; emptyPieceToReplaceWithTower: Piece }>()
);

export const grandRook = createAction(
  '[possibleMoves] grand Rook',
  props<{ tower: Piece; emptyPieceToReplaceWithTower: Piece }>()
);




export const emptyPossibleMoves = createAction(
  '[possibleMoves] Empty Possible Moves'
);

export const setBeforeAndAfterMovePosition = createAction(
  '[possibleMoves] Set Before Move Position',
  props<{ before: Coords, after: Coords }>()
);

export const setDefendingPossibleMoves = createAction(
  '[possibleMoves] set Defending Possible Moves',
  props<{ defendingCoords: Coords[], defendingPiece: Piece }>()
);


export const setDefendingPossibleMovesSuccess = createAction(
  '[possibleMoves] set Defending Possible Moves success',
  props<{ defendingCoords: Coords[], defendingPiece: Piece }>()
);

export const loadAllPossibleEnnemyMoves = createAction(
  '[possibleMoves] Load All Possible Ennemy Moves',
  props<{ turnToPlay: string; piece: Piece }>()
);

export const addThreateningMovesToPossibleEnnemyMoves = createAction(
  '[possibleMoves] add threatening piece to ennemy move',
  props<{ threateningMove: Coords[] }>()
);

export const loadAllPossibleEnnemyMovesSuccess = createAction(
  '[possibleMoves] Load All Possible Ennemy Moves Success',
  props<{ possibleEnnemyMoves: Array<Coords> }>()
);

export const loadPossibleMovesWhenCoveringKing = createAction(
  '[possibleMoves] Load Possible Moves when piece is covering King',
  props<{ possibleMoves: Coords[] }>()
);


export const loadCommonCoveringCoords = createAction(
  '[possibleMoves] Load covering Common Coords when piece is covering King',
  props<{ commonCoveringCoords: Coords[] }>()
);
