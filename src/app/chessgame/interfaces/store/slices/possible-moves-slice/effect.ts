import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ChessgameState } from '../../interfaces/chessgame.state';
import { PossibleMovesService } from 'src/app/chessgame/services/possible-moves.service';
import { loadAllPossibleEnnemyMoves, loadAllPossibleEnnemyMovesSuccess, loadPossibleMoves, loadPossibleMovesFailure, loadPossibleMovesSuccess, setDefendingPossibleMoves, setDefendingPossibleMovesSuccess } from './action';
import { Coords } from 'src/app/core/interfaces/coords';
import { changeTurnToPlay } from '../turn-to-play-slice/action';
import { getTurnToPlayColor } from '../turn-to-play-slice/selector';
import { selectThreateningPieces } from '../king-slice/selector';
import { Piece } from 'src/app/core/interfaces/piece';
import { PieceService } from 'src/app/chessgame/services/piece.service';
import { removeThreateningPiece } from '../king-slice/action';
import { selectAllPossibleEnnemyMoves } from './selector';
import { KingManagementService } from 'src/app/chessgame/services/king-management.service';
import { CoordsUtilService } from 'src/app/chessgame/services/coords-util.service';
import { selectLastPieceTaken } from '../last-piece-taken-slice/selector';

@Injectable()
export class PossibleMovesEffects {

  turnToPlay$: Observable<'white' | 'black'> = this.store.pipe(
    select(getTurnToPlayColor)
  );
  turnToPlay!: string;

  ThreateningPieces$: Observable<Piece[]> = this.store.pipe(
    select(selectThreateningPieces)
  );
  threateningPieces: Piece[] = [];


  selectAllPossibleEnnemyMoves$: Observable<Coords[]> = this.store.pipe(
    select(selectAllPossibleEnnemyMoves)
  );

  lastPieceTaken$: Observable<Piece | null> = this.store.pipe(
    select(selectLastPieceTaken)
  );
  lastPieceTaken!: Piece;

  allPossibleEnnemyMoves: Coords[] = [];


  constructor(
    private actions$: Actions,
    private store: Store<ChessgameState>,
    private possibleMovesService: PossibleMovesService,
    private cu: CoordsUtilService,
    private pieceService: PieceService,
    private kingManagementService: KingManagementService
  ) {

    this.ThreateningPieces$.subscribe((thrs: Piece[]) => this.threateningPieces = thrs);
    this.selectAllPossibleEnnemyMoves$.subscribe((ennemyMoves: Coords[]) => this.allPossibleEnnemyMoves = ennemyMoves);

    this.turnToPlay$.subscribe((t: string) => {
      this.turnToPlay = t;
    })

    this.lastPieceTaken$.subscribe((l: Piece | null) => {
      this.lastPieceTaken = <Piece>l;
    });

  }

  loadPossibleMoves$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(loadPossibleMoves),
      switchMap((action) =>
        this.possibleMovesService.showPossibleMoves(action.piece).pipe(
          map((possibleMoves: Coords[]) => {

            this.allPossibleEnnemyMoves = [
              ...this.allPossibleEnnemyMoves,
              ...this.possibleMovesService.showAllPossibleEnnemyMoves(action.piece)
            ];

            if (action.piece.pieceName === 'king') {
              possibleMoves = possibleMoves.filter((move: Coords) =>
                !this.allPossibleEnnemyMoves.some((ennemyMove: Coords) =>
                  PossibleMovesService.isSameCoords(move, ennemyMove)
                )
              );
              //console.log('allPossibleEnnemyMoves', this.allPossibleEnnemyMoves)

              const possibleMovesToPieces = this.cu.coordsToPiecesVariant(possibleMoves);
              const possibleMovesToCoordsWithoutThreat = this.cu.piecesToCoordsWithoutCoveredWithoutThreat(possibleMovesToPieces) as Coords[];

              
              possibleMoves = possibleMovesToCoordsWithoutThreat
              //piecesToCoords
            }
            console.log('possibleMoves', possibleMoves);

            return loadPossibleMovesSuccess({ possibleMoves, lastPieceClicked: action.piece });
          }),
          catchError((error) => of(loadPossibleMovesFailure({ error: error.message })))
        )
      )
    )
  );


  loadAllPossibleEnnemyMoves$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAllPossibleEnnemyMoves),
      map((action) => {
        const allPossibleEnnemyMoves: Coords[] = this.possibleMovesService.showAllPossibleEnnemyMoves(action.piece, );

        return loadAllPossibleEnnemyMovesSuccess({ possibleEnnemyMoves: allPossibleEnnemyMoves });

        return { type: '[NO_OP]' };  // No operation action. Replace with a real action if necessary.
      }),
      catchError((error) => of(loadPossibleMovesFailure({ error: error.message })))
    )
  );


  setDefendingPossibleMoves$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(setDefendingPossibleMoves),
      map((action) => {
        if((this.lastPieceTaken && this.pieceService.isSamePiece(this.lastPieceTaken, action.defendingPiece))){
          return { type: '[NO_OP]' };
        }
        return setDefendingPossibleMovesSuccess({ defendingCoords: action.defendingCoords, defendingPiece: action.defendingPiece  });
      }),
      catchError((error) => of(loadPossibleMovesFailure({ error: error.message })))
    )
  );

}
