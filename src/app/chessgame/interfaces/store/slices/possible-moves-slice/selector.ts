import { createSelector } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/chessgame.state';
import { PossibleMovesState } from '../../interfaces/possible-moves.state';

export const selectPossibleMovesState = createSelector(
  (state: ChessgameState) => state.possibleMoves,
  (possibleMoves) => possibleMoves
);

export const selectPossibleMoves = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.possibleMoves
);

export const selectallPossibleEnnemyMoves = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.allPossibleEnnemyMoves
);



export const selectAllPossibleMoves = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.allPossibleMoves
);

export const selectPossibleMovesLoading = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.isLoading
);

export const selectPossibleMovesError = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.error
);

export const selectLastPieceClicked = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.lastPieceClicked
);


export const selectCurrentPieceClicked = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.currentPieceClicked
);


export const selectBeforeMovePosition = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.beforeMovePosition
);

export const selectAfterMovePosition = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.afterMovePosition
);


export const selectAllPossibleEnnemyMoves = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.allPossibleEnnemyMoves
);

export const selectCommonCoveringCoords = createSelector(
  selectPossibleMovesState,
  (state: PossibleMovesState) => state.commonCoveringCoords
);