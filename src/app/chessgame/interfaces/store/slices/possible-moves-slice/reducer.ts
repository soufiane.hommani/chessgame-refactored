import { createReducer, on } from '@ngrx/store';
import { Coords } from 'src/app/core/interfaces/coords';
import { PossibleMovesState } from '../../interfaces/possible-moves.state';
import { addThreateningMovesToPossibleEnnemyMoves, emptyPossibleMoves, 
  loadAllPossibleEnnemyMovesSuccess, loadAllPossibleMoves, loadCommonCoveringCoords, 
  loadPossibleMovesSuccess, loadPossibleMovesWhenCoveringKing, setBeforeAndAfterMovePosition,
   setCurrentPieceClicked, setDefendingPossibleMoves, setDefendingPossibleMovesSuccess } from './action';
import { Store } from '@ngrx/store'; // Import the Store type
import { ChessgameState } from '../../interfaces/chessgame.state';


export const initialState: PossibleMovesState = {
  isLoading: false,
  possibleMoves: [],
  allPossibleMoves: [],
  commonCoveringCoords: [],
  allPossibleEnnemyMoves: [],
  beforeMovePosition: { x: -1, y: -1 },
  afterMovePosition: { x: -1, y: -1 },
  lastPieceClicked: null,
  currentPieceClicked: null,
  error: null,
 
};

export const possibleMovesReducer = createReducer(
  initialState,
  on(loadPossibleMovesSuccess, (state, { possibleMoves, lastPieceClicked }) => {
     // Check if lastPieceClicked exists and if its touched property is set to false
     
     let updatedLastPieceClicked = null;
     if (lastPieceClicked && lastPieceClicked.pieceName !== 'empty' && !lastPieceClicked.touched) {
       updatedLastPieceClicked = { ...lastPieceClicked, touched: true };
     } else {
       updatedLastPieceClicked = lastPieceClicked || state.lastPieceClicked;
     }

   
     

    return {
      ...state,
      possibleMoves,
      commonCoveringCoords: [],
      lastPieceClicked: updatedLastPieceClicked,
      isLoading: false,
      error: null,
    };
  }),
  on(setDefendingPossibleMovesSuccess, (state, { defendingCoords, defendingPiece }) => {
    let updatedLastPieceClicked = defendingPiece.pieceName !== 'empty' ? defendingPiece : state.lastPieceClicked;
    
    return {
      ...state,
      possibleMoves: defendingCoords,
      lastPieceClicked: updatedLastPieceClicked,
      isLoading: false,
      error: null,
    };
  }),
  on(setBeforeAndAfterMovePosition, (state, { before, after }) => ({
    ...state,
    beforeMovePosition: before,
    afterMovePosition: after,
  })),
  on(setCurrentPieceClicked, (state, {piece}) => ({
    ...state,
    currentPieceClicked: piece
  })),
  on(emptyPossibleMoves, (state) => ({
    ...state,
    possibleMoves: [], // Set possibleMoves array to an empty array
  })),
  on(loadAllPossibleMoves, (state, {allPossibleMoves}) => ({
    ...state,
    allPossibleMoves: allPossibleMoves, // Set allPossibleMoves 
  })),

  on(addThreateningMovesToPossibleEnnemyMoves, (state, {threateningMove}) => ({
    ...state,
    allPossibleEnnemyMoves: [...state.allPossibleEnnemyMoves, ...threateningMove], // Set allPossibleMoves 
  })),

  on(loadAllPossibleEnnemyMovesSuccess, (state, { possibleEnnemyMoves }) => ({
    ...state,
    allPossibleEnnemyMoves: possibleEnnemyMoves, // Update allPossibleEnnemyMoves with the payload
  })),
  on(loadCommonCoveringCoords, (state, { commonCoveringCoords }) => ({
    ...state,
    commonCoveringCoords: commonCoveringCoords,
    isLoading: false,
    error: null,
  })),

);


