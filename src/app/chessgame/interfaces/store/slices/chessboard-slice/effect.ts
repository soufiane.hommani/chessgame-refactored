import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, map, mergeMap, concatMap, switchMap } from 'rxjs/operators';
import {
  movePiece,
  setBeforeAndAfterMovePosition,
  movePieceSuccess,
  movePieceFailure,
  smallRook,
  grandRook,
} from '../possible-moves-slice/action';
import {  removeThreateningPiece } from '../king-slice/action';
import { Store, select } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/chessgame.state';
import { selectThreateningPieces } from '../king-slice/selector';
import { Piece } from 'src/app/core/interfaces/piece';
import { PieceService } from 'src/app/chessgame/services/piece.service';
import { getTurnToPlayColor } from '../turn-to-play-slice/selector';
import { selectChessboard } from './selector';
import { updateSmallRook } from './action';

@Injectable()
export class ChessboardEffects {

  ThreateningPieces$: Observable<Piece[]> = this.store.pipe(
    select(selectThreateningPieces)
  );
  threateningPieces: Piece[] = [];

  turnToPlay$: Observable<'white' | 'black'> = this.store.pipe(
    select(getTurnToPlayColor)
  );
  turnToPlay!: string;

  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  pieces: Array<Piece> = [];

  constructor(private actions$: Actions, private store: Store<ChessgameState>,
    private pieceService: PieceService) {
      this.turnToPlay$.subscribe((t: string) => {
        this.turnToPlay = t;
      })
    this.ThreateningPieces$.subscribe((thrs: Piece[]) => {
      this.threateningPieces = thrs
    });

    this.pieces$.subscribe((pcs: Piece[]) => {
      this.pieces = pcs
    });

  }

  movePiece$ = createEffect(() =>
    this.actions$.pipe(
      ofType(movePiece),
      concatMap(({ piece, lastPieceClicked }) =>
        of(
          setBeforeAndAfterMovePosition({
            before: lastPieceClicked!.coords,
            after: piece.coords,
          })
        ).pipe(
          switchMap(() => {

            //small rook
            if(lastPieceClicked?.pieceName === 'king' && piece.coords.x === (lastPieceClicked.coords.x + 2)){
              //small rook : we need to move the tower as well
              const tower: Piece = this.pieces.find(
                (p: Piece) => p.pieceName === 'tower' && p.coords.x === (lastPieceClicked.coords.x + 3) && p.color === lastPieceClicked.color) as Piece;

              const emptyPieceToReplaceWithTower = this.pieces.find(
                (p: Piece) => p.pieceName === 'empty' && p.coords.x === (lastPieceClicked.coords.x + 1) && p.coords.y === lastPieceClicked.coords.y) as Piece;

              this.store.dispatch(smallRook({tower, emptyPieceToReplaceWithTower}));
              
              // console.log('tower', tower)
              // console.log('emptyPieceToReplaceWithTower', emptyPieceToReplaceWithTower)
            }

            //grand rook
            if(lastPieceClicked?.pieceName === 'king' && piece.coords.x === (lastPieceClicked.coords.x - 2)){
              //grand rook : we need to move the tower as well
              const tower: Piece = this.pieces.find(
                (p: Piece) => p.pieceName === 'tower' && p.coords.x === (lastPieceClicked.coords.x - 4 ) && p.color === lastPieceClicked.color) as Piece;

              const emptyPieceToReplaceWithTower = this.pieces.find(
                (p: Piece) => p.pieceName === 'empty' && p.coords.x === (lastPieceClicked.coords.x - 1) && p.coords.y === lastPieceClicked.coords.y) as Piece;

              this.store.dispatch(grandRook({tower, emptyPieceToReplaceWithTower}));
              // console.log('tower', tower)
              // console.log('emptyPieceToReplaceWithTower', emptyPieceToReplaceWithTower)
            }

            this.store.dispatch( setBeforeAndAfterMovePosition({
              before: lastPieceClicked!.coords,
              after: piece.coords,
            }));
            
            //si action.piece fait partie de threatening pieces il faut la retirer en 
            //dispatchant
            const lastThreateninPiecePosition: Piece = this.threateningPieces.filter(thr => this.pieceService.isSamePiece(thr, lastPieceClicked!))[0];
            this.store.dispatch(removeThreateningPiece({ threateningPiece: lastPieceClicked! }))

            //dispatch un is king checked here

            return of(
              movePieceSuccess({
                piece,
                lastPieceClicked,
                newCoords: piece.coords,
              })
            );
          }),
          catchError((error: any) => of(movePieceFailure({ error: error.message })))
        )
      )
    )
  );
}
