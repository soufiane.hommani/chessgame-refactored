import { createReducer, on } from '@ngrx/store';
import { Piece } from 'src/app/core/interfaces/piece';
import initPieces from '../../../../../core/fixtures/pieces.json';
import * as chessgameAction from './action';
import * as possibleMoves from '../possible-moves-slice/action';
import { PossibleMovesService as pbm} from 'src/app/chessgame/services/possible-moves.service';
import { smallRook } from '../possible-moves-slice/action';


export const initialState = {

  pieces: <Piece[]>JSON.parse(JSON.stringify(initPieces)),
  smallRook: { status: false, rookableTower:     {"pieceName": "empty","color": false,"coords": { "x": -1, "y": -1 },"selected": false,"imagePath": ""} as Piece,
 },
  grandRook: { status: false, rookableTower:     {"pieceName": "empty","color": false,"coords": { "x": -1, "y": -1 },"selected": false,"imagePath": ""} as Piece,
 }

};

export const chessboardReducer = createReducer(
  initialState,
  on(chessgameAction.loadPieces, (state) => state),
  on(possibleMoves.movePiece, (state, { piece, lastPieceClicked }) => {
    //findTargetIndexInPieces
    //findLeavingIndexInPieces
    const indexTarget = pbm.findTargetIndexInPieces(state.pieces, piece);
    const leavingTarget =  pbm.findLeavingIndexInPieces(state.pieces, <Piece>lastPieceClicked);

    let updatedLastpieceClicked = {
      ...lastPieceClicked,
      coords: { ...lastPieceClicked!.coords, x: piece.coords.x, y: piece.coords.y },
      touched: true
    };

    let emptyPieceReplacingLeavingPiece =  {"pieceName": "empty","color": false,"coords": { "x": lastPieceClicked?.coords.x, "y": lastPieceClicked?.coords.y },"selected": false,"imagePath": ""};

    const updatedPieces: Piece[] = [...state.pieces];
    
    updatedPieces[indexTarget] = <Piece>updatedLastpieceClicked;
    updatedPieces[leavingTarget] = <Piece>emptyPieceReplacingLeavingPiece;

    return {
      ...state,
      pieces: updatedPieces
    };
  }),


  on( chessgameAction.updatePieceCoveredByAlly, (state, { updatedPiece }) => {

    //findTargetIndexInPieces
    //findLeavingIndexInPieces
    const indexTarget = pbm.findTargetIndexInPieces(state.pieces, updatedPiece);
    const updatedPieces: Piece[] = [...state.pieces];
    updatedPieces[indexTarget] = <Piece>updatedPiece;
   

    return {
      ...state,
      pieces: updatedPieces
    };
  }),



  on(possibleMoves.smallRook, (state, { tower, emptyPieceToReplaceWithTower }) => {
    // Find the indices of the tower and the empty spot in the pieces array
    const towerIndex = state.pieces.findIndex(p => p.pieceName === 'tower' && p.coords.x === tower.coords.x && p.coords.y === tower.coords.y);
    const emptySpotIndex = state.pieces.findIndex(p => p.coords.x === emptyPieceToReplaceWithTower.coords.x && p.coords.y === emptyPieceToReplaceWithTower.coords.y);

    // Create a new pieces array with updated tower and empty spot
    const updatedPieces = state.pieces.map((piece, index) => {
      if (index === towerIndex) {
        // Replace the tower with an empty piece
        return { ...emptyPieceToReplaceWithTower, coords: {...piece.coords} };
      } else if (index === emptySpotIndex) {
        // Move the tower to the new position
        return { ...tower, coords: {...emptyPieceToReplaceWithTower.coords}, touched: true };
      }
      return piece;
    });

    // Return the updated state
    return {
      ...state,
      pieces: updatedPieces,
    };
  }),

  on(possibleMoves.grandRook, (state, { tower, emptyPieceToReplaceWithTower }) => {
    // Find the indices of the tower and the empty spot in the pieces array
    const towerIndex = state.pieces.findIndex(p => p.pieceName === 'tower' && p.coords.x === tower.coords.x && p.coords.y === tower.coords.y);
    const emptySpotIndex = state.pieces.findIndex(p => p.coords.x === emptyPieceToReplaceWithTower.coords.x && p.coords.y === emptyPieceToReplaceWithTower.coords.y);

    // Create a new pieces array with updated tower and empty spot
    const updatedPieces = state.pieces.map((piece, index) => {
      if (index === towerIndex) {
        // Replace the tower with an empty piece
        return { ...emptyPieceToReplaceWithTower, coords: {...piece.coords} };
      } else if (index === emptySpotIndex) {
        // Move the tower to the new position
        return { ...tower, coords: {...emptyPieceToReplaceWithTower.coords}, touched: true };
      }
      return piece;
    });

    // Return the updated state
    return {
      ...state,
      pieces: updatedPieces,
    };
  }),

   // Handle small rook update
   on(chessgameAction.updateSmallRook, (state, { smallRookStatus, rookableTower  }) => ({
    ...state,
    smallRook: { status: smallRookStatus, rookableTower: rookableTower as Piece}
  })),

  // Handle grand rook update
  on(chessgameAction.updateGrandRook, (state, { grandRookStatus, rookableTower }) => ({
    ...state,
    grandRook: { status: grandRookStatus, rookableTower: rookableTower as Piece }
  })),
  // ... other on methods

);
