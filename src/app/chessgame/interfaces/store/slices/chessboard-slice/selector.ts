import { createSelector } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/chessgame.state';
;

// Select the chessboard state
export const selectChessboardState = (state: ChessgameState) => state.chessboard;

// Select the chessboard array from the state
export const selectChessboard = createSelector(
  selectChessboardState,
  (chessboardState) => chessboardState.pieces
);

// Selector for smallRook
export const selectSmallRook = createSelector(
  selectChessboardState,
  (chessboardState) => chessboardState.smallRook
);

// Selector for grandRook
export const selectGrandRook = createSelector(
  selectChessboardState,
  (chessboardState) => chessboardState.grandRook
);