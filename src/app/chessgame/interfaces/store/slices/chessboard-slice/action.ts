import { createAction, props } from '@ngrx/store';
import { Piece } from 'src/app/core/interfaces/piece';


export const loadPieces = createAction('[Chessboard] Load Pieces');

export const loadPiecesSuccess = createAction(
  '[Chessboard] Load Pieces Success',
  props<{ pieces: Piece[] }>()
);

export const loadPiecesFailure = createAction(
  '[Chessboard] Load Pieces Failure',
  props<{ error: any }>()
);

export const updatePieceCoveredByAlly = createAction(
  '[Chessboard] update Piece Covered By Ally',
  props<{ updatedPiece: Piece }>()
);

export const updateSmallRook = createAction(
  '[Chessboard] Update Small Rook',
  props<{ smallRookStatus: boolean; rookableTower: Piece | null }>()
);

export const updateGrandRook = createAction(
  '[Chessboard] Update Grand Rook',
  props<{ grandRookStatus: boolean; rookableTower: Piece | null }>()
);



