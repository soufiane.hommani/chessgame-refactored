import { createReducer, on } from '@ngrx/store';
import { Piece } from 'src/app/core/interfaces/piece';
import * as LastPieceTakenActions from './action';

export interface LastPieceTakenState {
  lastPieceTaken: Piece | null;
}

export const initialLastPieceTakenState: LastPieceTakenState = {
  lastPieceTaken: null
};

export const lastPieceTakenReducer = createReducer(
  initialLastPieceTakenState,
  on(LastPieceTakenActions.setLastPieceTaken, (state, { lastPieceTaken }) => {
    return {
      ...state,
      lastPieceTaken: lastPieceTaken
    };
  })
);
