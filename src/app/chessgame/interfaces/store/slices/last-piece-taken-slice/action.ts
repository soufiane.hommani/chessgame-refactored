import { createAction, props } from '@ngrx/store';
import { Piece } from 'src/app/core/interfaces/piece';

export const setLastPieceTaken = createAction(
  '[LastPieceTaken] Set Last Piece Taken',
  props<{ lastPieceTaken: Piece | null }>()
);
