import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LastPieceTakenState } from './reducer';

export const getLastPieceTakenState = createFeatureSelector<LastPieceTakenState>('lastPieceTaken');

export const selectLastPieceTaken = createSelector(
  getLastPieceTakenState,
  (state: LastPieceTakenState) => state?.lastPieceTaken
);
