import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TurnToPlayState } from '../../interfaces/turn-to-play-state';
import { LastPieceClickedState } from '../../interfaces/last-piece-clicked';

const getTurnToPlayState = createFeatureSelector<TurnToPlayState>('turnToPlay');
const getLastPieceClickedState = createFeatureSelector<LastPieceClickedState>('lastPieceClicked');

export const getTurnToPlay = createSelector(
  getTurnToPlayState,
  (state: TurnToPlayState) => state?.color
);


export const getLastPieceClicked = createSelector(
getLastPieceClickedState,
(state: LastPieceClickedState) => state?.lastPieceClicked
);
