import * as TurnToPlayActions from './actions';
import { createReducer, on } from "@ngrx/store";
import { TurnToPlayState } from '../../interfaces/turn-to-play-state';
import { LastPieceClickedState } from '../../interfaces/last-piece-clicked';

export const initialState: TurnToPlayState = {
  color: 'white'
};

export const initialLastPieceClickedState: LastPieceClickedState = {
  lastPieceClicked: null
};



export const lastPieceClickedReducer = createReducer(
  initialLastPieceClickedState,
  on(TurnToPlayActions.setLastPieceClicked, (state, { lastPieceClicked }) => {
    return {
      ...state,
      lastPieceClicked: lastPieceClicked
    }
  })
);
