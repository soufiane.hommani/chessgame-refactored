import { createAction, props } from '@ngrx/store';
import { Piece } from 'src/app/core/interfaces/piece';


export const setLastPieceClicked = createAction(
  '[LastPieceClicked] Set Last Piece Clicked',
  props<{ lastPieceClicked: Piece }>()
);
