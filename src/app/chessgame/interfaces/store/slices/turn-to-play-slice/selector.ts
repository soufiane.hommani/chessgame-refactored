import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TurnToPlayState } from '../../interfaces/turn-to-play-state';

export const getTurnToPlay = createFeatureSelector<TurnToPlayState>('turnToPlay');

export const getTurnToPlayColor = createSelector(
  getTurnToPlay,
  (state: TurnToPlayState) => state.color
);
