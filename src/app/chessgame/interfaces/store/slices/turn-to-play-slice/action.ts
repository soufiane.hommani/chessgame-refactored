import { createAction, props } from '@ngrx/store';

export const changeTurnToPlay = createAction(
  '[TurnToPlay] Change Turn To Play',
  props<{ color: 'white' | 'black' }>()
);
