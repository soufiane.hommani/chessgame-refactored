import { createReducer, on } from '@ngrx/store';
import { changeTurnToPlay } from './action';

export interface TurnToPlayState {
  color: 'white' | 'black';
}

export const initialState: TurnToPlayState = {
  color: 'white'
};

export const turnToPlayReducer = createReducer(
    initialState,
    on(changeTurnToPlay, (state, { color }) => {
      return { ...state, color };
    })
  );