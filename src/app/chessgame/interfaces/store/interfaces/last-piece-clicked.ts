import { Piece } from "src/app/core/interfaces/piece";

export interface LastPieceClickedState {

    lastPieceClicked: Piece | null;

}