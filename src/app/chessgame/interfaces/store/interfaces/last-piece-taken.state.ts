import { Piece } from "src/app/core/interfaces/piece";

export interface LastPieceTakenState {

    lastPieceTaken: Piece | null;
 
}