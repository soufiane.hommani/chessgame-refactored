import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';

export interface KingsState {
  isCheck: boolean;
  isCheckMate: boolean;
  kingCheckPossibleMove: Coords[];
}
