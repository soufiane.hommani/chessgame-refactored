import { Coords } from "src/app/core/interfaces/coords";
import { Piece } from "src/app/core/interfaces/piece";

export interface PossibleMovesState {
    isLoading: boolean;
    possibleMoves: Array<Coords>;
    allPossibleMoves: Array<Coords>;
    commonCoveringCoords: Array<Coords>;
    allPossibleEnnemyMoves: Array<Coords>;
    lastPieceClicked: Piece | null;
    currentPieceClicked: Piece | null;
    beforeMovePosition: Coords;
    afterMovePosition: Coords;
    error: string | null;
    
}
