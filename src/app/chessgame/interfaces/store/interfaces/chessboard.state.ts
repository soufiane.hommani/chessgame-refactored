import { Piece } from "src/app/core/interfaces/piece";

export interface ChessboardState {
    isLoading: boolean;
    pieces: Array<Piece>;
    error: string | null;
    hasMovedPiece: boolean;
    smallRook: { status: boolean; rookableTower: Piece | null };
    grandRook: { status: boolean; rookableTower: Piece | null };
}
