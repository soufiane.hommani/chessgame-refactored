import { ChessboardState } from "./chessboard.state";
import { KingStatusState } from "./king-status.state";
import { LastPieceTakenState } from "./last-piece-taken.state";
import { PossibleMovesState } from "./possible-moves.state";
import { TurnToPlayState } from "./turn-to-play-state";

export interface ChessgameState {
    chessboard: ChessboardState;
    possibleMoves: PossibleMovesState;
    turnToPlay: TurnToPlayState;
    lastPieceTaken: LastPieceTakenState;
    kingStatus: KingStatusState;
    
}
