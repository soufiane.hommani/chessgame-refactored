import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { KingsState } from './kings.state';

export interface KingStatusState {
  blackKing: KingsState;
  whiteKing: KingsState;
  kingChecked: Piece | null;
  threateningPieces: Piece[];
  emptyCoordsKingVSThreat: Coords[];
}
