import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { Store, select } from '@ngrx/store';
import { ChessgameState } from '../interfaces/store/interfaces/chessgame.state';
import { selectChessboard } from '../interfaces/store/slices/chessboard-slice/selector';

@Injectable()
export class CoordsUtilService {

  private readonly MIN_COORD = 0;
  private readonly MAX_COORD = 7;

  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  pieces: Array<Piece> = [];

  constructor(private store: Store<ChessgameState>) {
    this.pieces$.subscribe((p: Piece[]) => {
      this.pieces = p;
    });
  }

  //tableau d'espace entre piece cliquée et son roi : pieceClickedAllyKingSpace


  /**
   * Calculates the space (array of pieces) between the clicked piece and the ally king on the board.
   * This method assumes that the pieces are ordered linearly in the `scanningArray` as they would appear on a chessboard.
   *
   * @param {Piece[]} scanningArray - The array of pieces representing a line or diagonal on the board.
   * @param {Piece} pieceClicked - The piece that has been clicked/selected.
   * @returns {Piece[]} An array of pieces that are between the `pieceClicked` and its ally king,
   *                    including the cases where the king is before or after the clicked piece in the array.
   */
  calculatePieceClickedAllyKingSpace(scanningArray: Piece[], pieceClicked: Piece): Piece[] {
    // Find the indices of the pieceClicked and the ally king in the scanning array
    const pieceClickedIndex: number = scanningArray.findIndex(p => this.isSameCoords(p.coords, pieceClicked.coords));
    const allyKingIndex: number = scanningArray.findIndex(p => p.pieceName === 'king' && p.color === pieceClicked.color);

    // If the ally king is found after the piece clicked in the scanning array, return the pieces between
    if (allyKingIndex > pieceClickedIndex) {
      return scanningArray.slice(pieceClickedIndex + 1, allyKingIndex);
    }

    // If the ally king is found before the piece clicked in the scanning array, return the pieces between
    if (allyKingIndex < pieceClickedIndex) {
      return scanningArray.slice(allyKingIndex + 1, pieceClickedIndex);
    }

    // If the ally king index is the same as the piece clicked or isn't found, return an empty array
    return [];
  }

  /**
   * Calculates the space (array of pieces) between the clicked piece and the first enemy threat on the board
   * within the provided scanning array. This method identifies the first piece in the scanning array that is
   * not of the same color as the clicked piece and is not an empty square, treating it as a potential threat.
   *
   * @param {Piece[]} scanningArray - The array of pieces representing a line or diagonal on the board.
   * @param {Piece} pieceClicked - The piece that has been clicked/selected.
   * @returns {Piece[]} An array of pieces that are between the `pieceClicked` and the first piece
   *                    identified as a threat, exclusive of the clicked piece and the threat itself.
   */
  calculatePieceClickedFirstThreatSpace(scanningArray: Piece[], pieceClicked: Piece, firstThreat: Piece | null): Piece[] {

    if (!firstThreat) {
      return [];
    }
    // Find the index of the clicked piece and the first threat in the scanning array
    const pieceClickedIndex: number = scanningArray.findIndex(p => this.isSameCoords(p.coords, pieceClicked.coords));
    const firstThreatIndex: number = scanningArray.findIndex(p => this.isSameCoords(p.coords, firstThreat.coords));

    // If the first threat is found after the clicked piece in the array, return the pieces in between
    if (firstThreatIndex > pieceClickedIndex) {
      return scanningArray.slice(pieceClickedIndex + 1, firstThreatIndex);
    }

    // If the first threat is found before the clicked piece in the array, return the pieces in between
    if (firstThreatIndex < pieceClickedIndex) {
      return scanningArray.slice(firstThreatIndex + 1, pieceClickedIndex);
    }

    // If no threat is found or the threat index is the same as the clicked piece, return an empty array
    return [];
  }


  /**
 * Removes coordinates that fall outside the standard chessboard range.
 * @param coordsArray Array of coordinates to be filtered.
 * @returns Filtered array of coordinates within the valid chessboard range.
 */
  removeOutOfBoundsCoords(coordsArray: Coords[]): Coords[] {
    return coordsArray.filter(({ x, y }) =>
      x >= this.MIN_COORD && x <= this.MAX_COORD &&
      y >= this.MIN_COORD && y <= this.MAX_COORD
    );
  }

  /**
  * .
  */
  coordsToPiecesContainsAllyKing(coordsArray: Coords[], firstThreat: Piece): boolean {

    const { color } = firstThreat;

    const scanningArray: Piece[] = coordsArray
      .map(coord => this.pieces.find(piece => this.isSameCoords(piece.coords, coord)))
      .filter(piece => piece !== undefined) as Piece[];

    ////console.log('scanningArray', scanningArray)

    return  scanningArray.some((p: Piece) => p.pieceName === 'king' && p.color === color);

  }

  /**
  * 
  */
  coordsToPiecesContainsAllyCoveringPiece(coordsArray: Coords[], firstThreat: Piece, piecename: string): boolean {

    const { color } = firstThreat;

    const scanningArray: Piece[] = coordsArray
      .map(coord => this.pieces.find(piece => this.isSameCoords(piece.coords, coord)))
      .filter(piece => piece !== undefined) as Piece[];

    ////console.log('scanningArray', scanningArray)

    return  scanningArray.some((p: Piece) => p.pieceName === piecename && p.color === color);

  }

  /**
  * Converts an array of coordinates to corresponding pieces.
  * @param coordsArray - Array of coordinates.
  * @returns Array of pieces at the given coordinates.
  */
  coordsToPieces(coordsArray: Coords[], pieceClicked: Piece): Piece[] {

    const { color } = pieceClicked;


    const scanningArray: Piece[] = coordsArray
      .map(coord => this.pieces.find(piece => this.isSameCoords(piece.coords, coord)))
      .filter(piece => piece !== undefined) as Piece[];

    const scanningArrayContainingAllyKing: Piece[] = scanningArray.some((p: Piece) => p.pieceName === 'king' && p.color === color) ? scanningArray : []


    return scanningArrayContainingAllyKing;
  }


  piecesToCoords(piecesArray: Piece[]): Coords[]{
    return piecesArray.map(p => p.coords);
  }


  /**
 * Transforms an array of Piece objects into an array of Coords, excluding the coordinates of pieces that are marked as covered.
 * A piece is considered "covered" if its `isCovered` property is set to `true`.
 *
 * @param {Piece[]} piecesArray - The array of Piece objects to be transformed.
 * @returns {Coords[]} - An array of Coords corresponding to the pieces not covered.
 */
  piecesToCoordsWithoutCoveredWithoutThreat(piecesArray: Piece[]): Coords[] {
    return piecesArray
      .filter(p => !p.isCovered) // Filter out covered pieces
      .map(p => p.coords); // Map the remaining pieces to their coordinates
  }

  

  coordsToPiecesVariant(coordsArray: Coords[]): Piece[] {

    const scanningArray: Piece[] = coordsArray
      .map(coord => this.pieces.find(piece => this.isSameCoords(piece.coords, coord)))
      .filter(piece => piece !== undefined) as Piece[];

    return scanningArray;
  }

  diagonalesToPieces(diagonales: { [key: string]: Coords[] }): { [key: string]: Piece[] } {

    const topRightDiagonalPieces = this.coordsToPiecesVariant(diagonales.topRight);
    const topLeftDiagonalPieces = this.coordsToPiecesVariant(diagonales.topLeft);
    const bottomRightDiagonalPieces = this.coordsToPiecesVariant(diagonales.bottomRight);
    const bottomLeftDiagonalPieces = this.coordsToPiecesVariant(diagonales.bottomLeft);

    const diagonalesToPieces: { [key: string]: Piece[] } = {
      topLeft: topLeftDiagonalPieces,
      topRight: topRightDiagonalPieces,
      bottomLeft: bottomLeftDiagonalPieces,
      bottomRight: bottomRightDiagonalPieces
    };
  

    return diagonalesToPieces;

  }

  linesToPieces(lines: { [key: string]: Coords[] }): { [key: string]: Piece[] } {
    const topVerticalPieces = this.coordsToPiecesVariant(lines.topVertical);
    const bottomVerticalPieces = this.coordsToPiecesVariant(lines.bottomVertical);
    const leftHorizontalPieces = this.coordsToPiecesVariant(lines.leftHorizontal);
    const rightHorizontalPieces = this.coordsToPiecesVariant(lines.rightHorizontal);
  
    const linesToPieces: { [key: string]: Piece[] } = {
      topVertical: topVerticalPieces,
      bottomVertical: bottomVerticalPieces,
      leftHorizontal: leftHorizontalPieces,
      rightHorizontal: rightHorizontalPieces
    };
  
    return linesToPieces;
  }


  linesAndDiagonalesToPieces(linesAndDiagonales: { [key: string]: Coords[] }): { [key: string]: Piece[] } {

    const topVerticalPieces = this.coordsToPiecesVariant(linesAndDiagonales.topVertical);
    const bottomVerticalPieces = this.coordsToPiecesVariant(linesAndDiagonales.bottomVertical);
    const leftHorizontalPieces = this.coordsToPiecesVariant(linesAndDiagonales.leftHorizontal);
    const rightHorizontalPieces = this.coordsToPiecesVariant(linesAndDiagonales.rightHorizontal);

    const topRightDiagonalPieces = this.coordsToPiecesVariant(linesAndDiagonales.topRight);
    const topLeftDiagonalPieces = this.coordsToPiecesVariant(linesAndDiagonales.topLeft);
    const bottomRightDiagonalPieces = this.coordsToPiecesVariant(linesAndDiagonales.bottomRight);
    const bottomLeftDiagonalPieces = this.coordsToPiecesVariant(linesAndDiagonales.bottomLeft);
  
    const linesAndDiagonalesToPieces: { [key: string]: Piece[] } = {
      topLeft: topLeftDiagonalPieces,
      topRight: topRightDiagonalPieces,
      bottomLeft: bottomLeftDiagonalPieces,
      bottomRight: bottomRightDiagonalPieces,
      topVertical: topVerticalPieces,
      bottomVertical: bottomVerticalPieces,
      leftHorizontal: leftHorizontalPieces,
      rightHorizontal: rightHorizontalPieces
    };
  
    return linesAndDiagonalesToPieces;
  }
  


  
/*        8  . . . . . . . 
          7  . bB . . . . . 
          6  . . . bK . . . 
          5  . . . . . . . . 
          4  . . . . . . bP 
          3  . . . . . . .  wK
          2  . . . . . . . .
          1  . . . . . . . .
             a b c d e f g h
          wK should be able to take black pawn (bP)
*/
  isBishopUnobstructedAfterEmpty(diagonalesToPieces: { [key: string]: Piece[] }, firstThreatColor: string): boolean {
    for (const diagonal in diagonalesToPieces) {
      let emptyPassed = false;
  
      for (const piece of diagonalesToPieces[diagonal]) {


        if (!emptyPassed && piece.pieceName !== 'empty' && !(piece.pieceName === 'bishop' || piece.pieceName === 'queen')) {
          // Found a non-empty piece that is not a tower or queen after empty spaces
          break; // Break out of the current line check
        }

        if (!emptyPassed && (piece.pieceName === 'bishop' || piece.pieceName === 'queen') && piece.color === firstThreatColor ) {
          // Mark that we have passed an empty space
          emptyPassed = true;
          continue;
        }

        if (!emptyPassed && !(piece.pieceName === 'bishop' || piece.pieceName === 'queen') && piece.color === firstThreatColor ) {
          // Mark that we have passed an empty space
          emptyPassed = true;
          continue;
        }

        if (!emptyPassed && piece.pieceName === 'empty') {
          // Mark that we have passed an empty space
          emptyPassed = true;
          continue;
        }
  
        if (emptyPassed && piece.pieceName !== 'empty' && piece.pieceName !== 'bishop') {
          // Found a non-empty piece that is not a bishop after empty spaces
          break; // Break out of the current diagonal check
        }
  
        if (emptyPassed && piece.pieceName === 'bishop') {
          // Found a bishop right after empty spaces without any other piece in between
          return true; // Found an unobstructed bishop
        }
        if (emptyPassed && piece.pieceName !== 'empty' && !(piece.pieceName === 'bishop' || piece.pieceName === 'queen')) {
          // Found a non-empty piece that is not a bishop or queen after empty spaces
          break; // Break out of the current line check
        }
        if (emptyPassed && piece.pieceName !== 'empty' && !(piece.pieceName === 'bishop' || piece.pieceName === 'queen')) {
          ////console.log('passe la 4')
          // Found a non-empty piece that is not a bishop or queen after empty spaces
          break; // Break out of the current line check
        }
        if (emptyPassed && (piece.pieceName === 'bishop' || piece.pieceName === 'queen')) {
          // Found a bishop right after empty spaces without any other piece in between
          ////console.log('passe la 5')
          return true; // Found an unobstructed bishop
        }
      }
    }
  
    // Checked all diagonals and no unobstructed bishop found after empty spaces
    return false;
  }
  

  /*         . . bT . . . . 
          7  . . . . . . .
          6  . . bK . . . .
          5  . . . . . . . . 
          4  . . . . . . . .
          3  . . bP . . . .
          2  . . . wK . . . 
          1  . . . . . . . .
             a b c d e f g h
          wK should be able to take black pawn (bP)
*/
  isTowerUnobstructedAfterEmpty(linesToPieces: { [key: string]: Piece[] }, firstThreatColor: string): boolean {
    for (const line in linesToPieces) {
      let emptyPassed = false;
  
      for (const piece of linesToPieces[line]) {
        if (!emptyPassed && piece.pieceName !== 'empty' && !(piece.pieceName === 'tower' || piece.pieceName === 'queen')) {
          // Found a non-empty piece that is not a tower or queen after empty spaces
          break; // Break out of the current line check
        }
        if (!emptyPassed && (piece.pieceName === 'tower' || piece.pieceName === 'queen') && piece.color === firstThreatColor ) {
          // Mark that we have passed an empty space
          emptyPassed = true;
          continue;
        }
        if (!emptyPassed && !(piece.pieceName === 'tower' || piece.pieceName === 'queen') && piece.color === firstThreatColor ) {
          // Mark that we have passed an empty space
          emptyPassed = true;
          continue;
        }
        if (!emptyPassed && piece.pieceName === 'empty') {
          // Mark that we have passed an empty space
          emptyPassed = true;
          continue;
        }
        if (emptyPassed && piece.pieceName !== 'empty' && !(piece.pieceName === 'tower' || piece.pieceName === 'queen')) {
          // Found a non-empty piece that is not a tower or queen after empty spaces
          break; // Break out of the current line check
        }
        if (emptyPassed && piece.pieceName !== 'empty' && !(piece.pieceName === 'tower' || piece.pieceName === 'queen')) {
          ////console.log('passe la 4')
          // Found a non-empty piece that is not a tower or queen after empty spaces
          break; // Break out of the current line check
        }
        if (emptyPassed && (piece.pieceName === 'tower' || piece.pieceName === 'queen')) {
          // Found a tower right after empty spaces without any other piece in between
          ////console.log('passe la 5')
          return true; // Found an unobstructed tower
        }
      }
    }
  
    // Checked all lines and no unobstructed tower found after empty spaces
    return false;
  }


  isKingThreatCovered(linesAndDiagonalesToPieces: { [key: string]: Piece[] }, threatColor: string): boolean {
    // Helper function to check coverage within a single direction (line or diagonal)
    const isDirectionCovered = (piecesArray: Piece[], coveringPieces: string[]): boolean => {
      for (const piece of piecesArray) {
        // If the first piece is a queen or rook/tower of the same color, it's covered
        if (piece.color === threatColor && coveringPieces.includes(piece.pieceName)) {
          return true; // Threat is covered
        }
  
        // If the first piece is not empty and not the same type as the covering piece, the path is obstructed
        if (piece.pieceName !== 'empty' && !coveringPieces.includes(piece.pieceName)) {
          return false;
        }
  
        // If the first piece is empty, continue to the next piece
        if (piece.pieceName === 'empty') {
          continue;
        }
      }
  
      // No covering piece found at all
      return false;
    };
  
    // Define the types of pieces that can cover in each direction
    const diagonalsCoveringPieces = ['bishop', 'queen'];
    const linesCoveringPieces = ['tower', 'queen'];
  
    // Check each diagonal for coverage
    for (const direction of ['topLeft', 'topRight', 'bottomLeft', 'bottomRight']) {
      const diagonal = linesAndDiagonalesToPieces[direction];
      if (isDirectionCovered(diagonal, diagonalsCoveringPieces)) return true;
    }
  
    // Check each line for coverage
    for (const direction of ['topVertical', 'bottomVertical', 'leftHorizontal', 'rightHorizontal']) {
      const line = linesAndDiagonalesToPieces[direction];
      if (isDirectionCovered(line, linesCoveringPieces)) return true;
    }
  
    // If none of the directions are covered, return false
    return false;
  }
  



  /**
 * Checks if two sets of coordinates are the same.
 * @param c1 - First coordinate set.
 * @param c2 - Second coordinate set.
 * @returns True if coordinates are the same, false otherwise.
 */
  isSameCoords(c1: Coords, c2: Coords): boolean {
    return c1 && c2 && c1.x === c2.x && c1.y === c2.y;
  }

  /**
 * Determines if two coordinates are on the same diagonal.
 * @param c1 - The first set of coordinates.
 * @param c2 - The second set of coordinates.
 * @returns True if the coordinates are diagonal, false otherwise.
 */
  isDiagonal(c1: Coords, c2: Coords): boolean {
    return Math.abs(c1.x - c2.x) === Math.abs(c1.y - c2.y);
  }

  /**
   * Determines if two coordinates are on the same line vertically or horizontally.
   * @param c1 - The first set of coordinates.
   * @param c2 - The second set of coordinates.
   * @returns True if the coordinates are on the same line, false otherwise.
   */
  isSameLine(c1: Coords, c2: Coords): boolean {
    return c1.x === c2.x || c1.y === c2.y;
  }

  /**
   * Determines if the path between two coordinates is clear of other pieces.
   * @param start - The starting coordinates.
   * @param end - The ending coordinates.
   * @returns True if the path is clear, false if obstructed.
   */
  isPathClear(start: Coords, end: Coords): boolean {
    // This method needs to account for the pieces on the board,
    // which likely requires access to the current game state.
    // For now, this is a placeholder to illustrate structure.
    return true; // Placeholder response
  }

  /**
   * Calculates all possible diagonal coordinates from a given starting point in a given direction.
   * @param start - The starting coordinates.
   * @param direction - The direction of the diagonal ('/' or '\\').
   * @returns An array of coordinates representing the diagonal.
   */
  calculateDiagonalCoords(start: Coords, direction: '/' | '\\'): Coords[] {
    // //console.log('start', start)

    let coords: Coords[] = [];


    for (let i = 1; i <= 8; i++) {
      if (direction === '/') {

        coords.push({ x: start.x - i, y: start.y + i });
        coords.push({ x: start.x + i, y: start.y - i });

      } else {

        coords.push({ x: start.x + i, y: start.y + i });
        coords.push({ x: start.x - i, y: start.y - i });

      }
    }

    coords.push({ x: start.x, y: start.y });

    coords = this.removeOutOfBoundsCoords(coords);

    if (direction === '\\')
      coords = this.sortCoordsByYDesc(coords);

    if (direction === '/')
      coords = this.sortCoordsByYAsc(coords);

    ////console.log('returned coords', coords)


    return coords;
  }

  /**
 * Sorts an array of coordinates by the Y value in descending order.
 * @param {Coords[]} coordsArray - The array of coordinates to sort.
 * @returns {Coords[]} The sorted array of coordinates.
 */
  sortCoordsByYDesc(coordsArray: Coords[]): Coords[] {
    return coordsArray.sort((a, b) => b.y - a.y);
  }

  /**
 * Sorts an array of coordinates by the Y value in ascending order.
 * @param {Coords[]} coordsArray - The array of coordinates to sort.
 * @returns {Coords[]} The sorted array of coordinates.
 */
  sortCoordsByYAsc(coordsArray: Coords[]): Coords[] {
    return coordsArray.sort((a, b) => a.y - b.y);
  }

  /**
   * Calculates all possible vertical or horizontal coordinates from a given starting point.
   * @param start - The starting coordinates.
   * @param direction - The direction of the line ('vertical' or 'horizontal').
   * @returns An array of coordinates representing the line.
   */
  calculateLineCoords(start: Coords, direction: 'vertical' | 'horizontal'): Coords[] {
    let coords: Coords[] = [];
    for (let i = 0; i <= 8; i++) {
      if (direction === 'vertical') {
        coords.push({ x: start.x, y: i });
      } else {
        coords.push({ x: i, y: start.y });
      }
    }
    return coords;
  }


  /**
* Finds the coordinates of the king for a given color.
* @param color - The color of the king to find.
* @returns The coordinates of the king if found, otherwise null.
*/
  findKingCoords(color: 'white' | 'black'): Coords | null {
    const kingPiece = this.pieces.find(piece => piece.pieceName === 'king' && piece.color === color);
    return kingPiece ? kingPiece.coords : null;
  }


  /**
 * Finds common coordinates between the space of the clicked piece to the first threat
 * and the possible moves of the clicked piece.
 * @param {Piece[]} pieceClickedFirstThreatSpace - Array of pieces representing the space between the clicked piece and the first threat.
 * @param {Coords[]} piecePossibleMoves - Array of coordinates representing the possible moves of the clicked piece.
 * @returns {Coords[] | false} An array of common coordinates, or false if no common coordinates are found.
 */
  findCommonCoords(pieceClickedFirstThreatSpace: Piece[], piecePossibleMoves: Coords[]): Coords[] {
    // Extract the coordinates from the pieces in the threat space
    const threatSpaceCoords = pieceClickedFirstThreatSpace.map(piece => piece.coords);

    // Find common coordinates between the threat space and possible moves
    const commonCoords = piecePossibleMoves.filter(move =>
      threatSpaceCoords.some(coords => this.isSameCoords(coords, move))
    );

    // Return the common coordinates or false if there are none
    return commonCoords.length > 0 ? commonCoords : [];
  }

  /**
   * Finds common coordinates between two arrays of coordinates.
   * @param {Coords[]} firstCoordsArray - The first array of coordinates.
   * @param {Coords[]} secondCoordsArray - The second array of coordinates.
   * @returns {Coords[]} An array of coordinates that are common to both input arrays.
   */
  findCommonCoordinates(firstCoordsArray: Coords[], secondCoordsArray: Coords[]): Coords[] {
    // Find common coordinates by filtering the first array
    const commonCoords = firstCoordsArray.filter(coord1 =>
      secondCoordsArray.some(coord2 => this.isSameCoords(coord1, coord2))
    );

    // Return the common coordinates or an empty array if there are none
    return commonCoords;
  }




}
