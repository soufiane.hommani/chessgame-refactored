import { Injectable } from '@angular/core';
import { BishopService } from './bishop.service';
import { Store } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { MovablePiece } from '../../interfaces/movable-piece';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { PieceService } from '../piece.service';

@Injectable({
  providedIn: 'root'
})
export class TowerService implements MovablePiece {

  constructor(private pieceService: PieceService) {

  }


  showPossibleAbsoluteCoords(piece: Piece): Coords[] {

    const { coords, color } = piece;
    const { x, y } = coords;
    
    let possibleCoords: Array<Coords> = [];

    let top = [
      { x: x, y: y - 1 },
      { x: x, y: y - 2 },
      { x: x, y: y - 3 },
      { x: x, y: y - 4 },
      { x: x, y: y - 5 },
      { x: x, y: y - 6 },
      { x: x, y: y - 7 },
    ]

    let right = [
      { x: x + 1, y: y },
      { x: x + 2, y: y },
      { x: x + 3, y: y },
      { x: x + 4, y: y },
      { x: x + 5, y: y },
      { x: x + 6, y: y },
      { x: x + 7, y: y },
    ]

    let bottom = [
      { x: x, y: y + 1 },
      { x: x, y: y + 2 },
      { x: x, y: y + 3 },
      { x: x, y: y + 4 },
      { x: x, y: y + 5 },
      { x: x, y: y + 6 },
      { x: x, y: y + 7 },
    ]

    let left = [
      { x: x - 1, y: y },
      { x: x - 2, y: y },
      { x: x - 3, y: y },
      { x: x - 4, y: y },
      { x: x - 5, y: y },
      { x: x - 6, y: y },
      { x: x - 7, y: y },
    ]

    

    top = this.pieceService.removeBeyondAllyIndex(top,  this.pieceService.findAllyIndex(top, <string>color));
    top = this.pieceService.removeBeyondAllyIndex(top,  this.pieceService.findEnnemyIndex(top, <string>color));

    
    right = this.pieceService.removeBeyondAllyIndex(right,  this.pieceService.findAllyIndex(right, <string>color));
    right = this.pieceService.removeBeyondAllyIndex(right,  this.pieceService.findEnnemyIndex(right, <string>color));


    
    bottom = this.pieceService.removeBeyondAllyIndex(bottom,  this.pieceService.findAllyIndex(bottom, <string>color));
    bottom = this.pieceService.removeBeyondAllyIndex(bottom,  this.pieceService.findEnnemyIndex(bottom, <string>color));

    left = this.pieceService.removeBeyondAllyIndex(left,  this.pieceService.findAllyIndex(left, <string>color));
    left = this.pieceService.removeBeyondAllyIndex(left,  this.pieceService.findEnnemyIndex(left, <string>color));

    possibleCoords = [ ...top, ...right, ...bottom, ...left ];

    return possibleCoords;
  }

  showPossibleAbsoluteBeyondEnnemyCoords(piece: Piece): Coords[] {

    const { coords, color } = piece;
    const { x, y } = coords;
    
    let possibleCoords: Array<Coords> = [];

    let top = [
      { x: x, y: y - 1 },
      { x: x, y: y - 2 },
      { x: x, y: y - 3 },
      { x: x, y: y - 4 },
      { x: x, y: y - 5 },
      { x: x, y: y - 6 },
      { x: x, y: y - 7 },
    ]

    let right = [
      { x: x + 1, y: y },
      { x: x + 2, y: y },
      { x: x + 3, y: y },
      { x: x + 4, y: y },
      { x: x + 5, y: y },
      { x: x + 6, y: y },
      { x: x + 7, y: y },
    ]

    let bottom = [
      { x: x, y: y + 1 },
      { x: x, y: y + 2 },
      { x: x, y: y + 3 },
      { x: x, y: y + 4 },
      { x: x, y: y + 5 },
      { x: x, y: y + 6 },
      { x: x, y: y + 7 },
    ]

    let left = [
      { x: x - 1, y: y },
      { x: x - 2, y: y },
      { x: x - 3, y: y },
      { x: x - 4, y: y },
      { x: x - 5, y: y },
      { x: x - 6, y: y },
      { x: x - 7, y: y },
    ]

    

    top = this.pieceService.removeBeyondAllyIndex(top,  this.pieceService.findAllyIndex(top, <string>color));

    
    right = this.pieceService.removeBeyondAllyIndex(right,  this.pieceService.findAllyIndex(right, <string>color));


    
    bottom = this.pieceService.removeBeyondAllyIndex(bottom,  this.pieceService.findAllyIndex(bottom, <string>color));

    left = this.pieceService.removeBeyondAllyIndex(left,  this.pieceService.findAllyIndex(left, <string>color));

    possibleCoords = [ ...top, ...right, ...bottom, ...left ];


    return possibleCoords;
  }
}
