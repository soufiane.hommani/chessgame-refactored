import { Injectable } from '@angular/core';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { MovablePiece } from '../../interfaces/movable-piece';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { selectChessboard } from '../../interfaces/store/slices/chessboard-slice/selector';
import { PossibleMovesService as pbm } from 'src/app/chessgame/services/possible-moves.service';
import { CoordsUtilService } from '../coords-util.service';

@Injectable({
  providedIn: 'root',
})
export class PawnService implements MovablePiece {




  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  pieces: Piece[] = [];

  constructor(private store: Store<ChessgameState>, private coordUtilService: CoordsUtilService
    ) {
    this.pieces$.subscribe((p: Piece[]) => {
      this.pieces = p;
    });
  }


  /**
 * Calculates and returns the possible absolute (non-diagonal) coordinates for a given pawn's movement.
 * These coordinates represent both standard forward moves and captures.
 *
 * @remarks
 * This method factors in the following conditions:
 * - A pawn moves forward one square, but on its first move, it has the option of moving forward two squares.
 * - Pawns cannot move backwards.
 * - Pawns capture diagonally.
 * - It assumes a standard chessboard setup where the black pieces are at the top 
 *   and white pieces are at the bottom.
 * - The method filters out moves where an enemy piece is blocking the pawn's standard forward movement.
 * - It checks for the presence of enemy pieces on the diagonal squares where pawns can capture.
 *
 * @param piece - The pawn for which to determine possible moves.
 * @returns An array of coordinates representing possible movements (both forward and captures).
 *
 * @example
 * const pawn: Piece = {
 *   pieceName: 'pawn',
 *   color: 'white',
 *   coords: { x: 4, y: 4 },
 *   touched: false
 * };
 *
 * const possibleMoves = pawnService.showPossibleAbsoluteCoords(pawn);
 * // possibleMoves might return [{ x: 4, y: 3 }, { x: 4, y: 2 }, { x: 3, y: 3 }, { x: 5, y: 3 }]
 */
  showPossibleAbsoluteCoords(piece: Piece): Array<Coords> {

    let possibleCoords: Array<Coords> = [];
    const { pieceName, color, coords, touched } = piece;
    const { x, y } = coords;

    switch (color) {
      case 'white':
        if (!touched) {
          possibleCoords = [
            { x: x, y: y - 2 },
            { x: x, y: y - 1 },
          ];
        } else {
          possibleCoords = [{ x: x, y: y - 1 }];
        }


        possibleCoords = possibleCoords.filter((c: Coords) => !this.isEnnemyPiecePresent(c, 'white'));

        const possibleEnnemyCoordsLeft = { x: x - 1, y: y - 1 };
        const possibleEnnemyCoordsRight = { x: x + 1, y: y - 1 };


        if (pbm.isCoordsValid(possibleEnnemyCoordsLeft) && this.isEnnemyPiecePresent(possibleEnnemyCoordsLeft, 'white')) {
          possibleCoords.push(possibleEnnemyCoordsLeft);
        }

        if (pbm.isCoordsValid(possibleEnnemyCoordsRight) && this.isEnnemyPiecePresent(possibleEnnemyCoordsRight, 'white')) {
          possibleCoords.push(possibleEnnemyCoordsRight);
        }
        break;

      case 'black':
        if (!touched) {
          possibleCoords = [
            { x: x, y: y + 2 },
            { x: x, y: y + 1 },
          ];
        } else {
          possibleCoords = [{ x: x, y: y + 1 }];
        }

        possibleCoords = possibleCoords.filter((c: Coords) => !this.isEnnemyPiecePresent(c, 'black'));

        const possibleEnnemyCoordsLeftBlack = { x: x - 1, y: y + 1 };
        const possibleEnnemyCoordsRightBlack = { x: x + 1, y: y + 1 };

        if (pbm.isCoordsValid(possibleEnnemyCoordsLeftBlack) && this.isEnnemyPiecePresent(possibleEnnemyCoordsLeftBlack, 'black')) {
          possibleCoords.push(possibleEnnemyCoordsLeftBlack);
        }

        if (pbm.isCoordsValid(possibleEnnemyCoordsRightBlack) && this.isEnnemyPiecePresent(possibleEnnemyCoordsRightBlack, 'black')) {
          possibleCoords.push(possibleEnnemyCoordsRightBlack);
        }

        break;
    }

    const possibleMovesToPiece = this.coordUtilService.coordsToPiecesVariant(possibleCoords);
    
    if(possibleMovesToPiece.some((p: Piece )=> (p.coords.x === piece.coords.x) && p.pieceName !== 'empty' )){

      //si pion blanc et jamais touché
      if(!piece.touched){

        //index piece alliée:
        const allyIndex = possibleMovesToPiece.findIndex((p: Piece) => p.pieceName !== 'empty')

        if(allyIndex == 1){
          possibleCoords = possibleCoords.filter((c: Coords) => c.x !== piece.coords.x);
        }
        
      }

    }

    return possibleCoords;
  }

  isEnnemyPiecePresent(possibleEnnemyCoords: Coords, color: string): boolean {
    //trouver la piece dans pieces dont la coords === possibleEnnemyCoords
    const ennemy = this.pieces.find((p: Piece) =>
      pbm.isSameCoords(p.coords, possibleEnnemyCoords)
    )!;
    return ennemy.pieceName !== 'empty' && ennemy.color !== color;
  }



  /**
 * Calculates and returns the possible diagonal coordinates for a given pawn.
 * These coordinates represent all diagonal moves a pawn can potentially make, 
 * without checking for the presence of any pieces.
 *
 * @param piece - The pawn for which to determine possible diagonal moves.
 * @returns An array of coordinates representing all possible diagonal moves.
 */
showPossibleDiagonaleAbsoluteCoords(piece: Piece): Coords[] {


  let possibleDiagonalCoords: Coords[] = [];
  const { color, coords } = piece;
  const { x, y } = coords;

  switch (color) {
    case 'white':
      possibleDiagonalCoords = [
        { x: x - 1, y: y - 1 },  // Diagonal left-up for white pawn
        { x: x + 1, y: y - 1 }   // Diagonal right-up for white pawn
      ];
      break;

    case 'black':
      possibleDiagonalCoords = [
        { x: x - 1, y: y + 1 },  // Diagonal left-down for black pawn
        { x: x + 1, y: y + 1 }   // Diagonal right-down for black pawn
      ];
      break;
  }

  // Filtering the coords to ensure they are within valid boundaries
  possibleDiagonalCoords = possibleDiagonalCoords.filter(pbm.isCoordsValid);
  return possibleDiagonalCoords;
}


}
