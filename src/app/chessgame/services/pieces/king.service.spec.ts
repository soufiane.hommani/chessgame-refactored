import { TestBed } from '@angular/core/testing';

import { PiecesKingService } from './pieces.king.service';

describe('PiecesKingService', () => {
  let service: PiecesKingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PiecesKingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
