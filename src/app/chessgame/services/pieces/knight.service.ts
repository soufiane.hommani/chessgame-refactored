import { Injectable } from '@angular/core';
import { MovablePiece } from '../../interfaces/movable-piece';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';

@Injectable({
  providedIn: 'root'
})
export class KnightService implements MovablePiece {

  constructor() { }



  showPossibleAbsoluteCoords(piece: Piece): Coords[] {
    let possibleCoords: Array<Coords> = [];

    const { coords } = piece;
    const { x, y } = coords;

    possibleCoords = [

      { x: x - 1, y: y - 2 },
      { x: x - 1, y: y + 2 },

      { x: x - 2, y: y - 1 },
      { x: x - 2, y: y + 1 },


      { x: x + 1, y: y - 2 },
      { x: x + 1, y: y + 2 },

      { x: x + 2, y: y - 1 },
      { x: x + 2, y: y + 1 },

    ]

    return possibleCoords;
  }






}
