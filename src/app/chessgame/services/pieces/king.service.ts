import { Injectable, OnInit } from '@angular/core';
import { PieceService } from '../piece.service';
import { MovablePiece } from '../../interfaces/movable-piece';
import { Piece } from 'src/app/core/interfaces/piece';
import { Coords } from 'src/app/core/interfaces/coords';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectChessboard } from '../../interfaces/store/slices/chessboard-slice/selector';
import { selectCheckedKing, selectFirstThreateningPiece, selectThreateningPieces } from '../../interfaces/store/slices/king-slice/selector';
import { emptyThreateningPieces, storeEmptyCoordsBetweenKingAndThreat, storeThreateningPieces } from '../../interfaces/store/slices/king-slice/action';
import { PossibleMovesService } from '../possible-moves.service';
import { CoordsUtilService } from '../coords-util.service';
import { selectallPossibleEnnemyMoves } from '../../interfaces/store/slices/possible-moves-slice/selector';
import { updateGrandRook, updateSmallRook } from '../../interfaces/store/slices/chessboard-slice/action';

export interface CheckedConfiguration {
  threateningPieces: Piece[],
  emptyCoords: Coords[]
}


@Injectable({
  providedIn: 'root',
})
export class KingService implements MovablePiece {

  kingChecked$: Observable<Piece> = this.store.pipe(select(selectCheckedKing));
  kingChecked?: Piece;

  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  pieces: Array<Piece> = [];

  ThreateningPieces$: Observable<Piece[]> = this.store.pipe(
    select(selectThreateningPieces)
  );
  threateningPieces: Piece[] = [];

  allPossibleEnnemyMoves$: Observable<Coords[]> = this.store.pipe(
    select(selectallPossibleEnnemyMoves)
  );
  allPossibleEnnemyMoves: Coords[] = [];


  firstThreateningPiece$: Observable<Piece> = this.store.pipe(select(selectFirstThreateningPiece));
  firstThreateningPiece?: Piece;

  checkedConfiguration: CheckedConfiguration = { threateningPieces: [], emptyCoords: [] };

  constructor(
    private pieceService: PieceService,
    private store: Store<ChessgameState>,
    private coordUtilService: CoordsUtilService
  ) {

    this.ThreateningPieces$.subscribe((thrs: Piece[]) => this.threateningPieces = thrs);


    this.allPossibleEnnemyMoves$.subscribe((all: Coords[]) => this.allPossibleEnnemyMoves = all);


    this.firstThreateningPiece$.subscribe((thr: Piece) => {
      this.firstThreateningPiece = thr;

      //console.log('thr', thr)



      if(this.kingChecked && this.threateningPieces.length){
        let checkedEmpty = this.getCoordsBetweenCheckedKingAndFirstThreateningPiece(<Piece>this.kingChecked, this.firstThreateningPiece)
        this.store.dispatch(storeEmptyCoordsBetweenKingAndThreat({empty: checkedEmpty}))
      }
    
    
    
    });


    this.pieces$.subscribe((p: Piece[]) => {
      this.pieces = p;
    });

    this.kingChecked$.subscribe((k: Piece) => {
      this.kingChecked = k;

      this.store.dispatch(emptyThreateningPieces(null));

      if (!k) {
        return;
      }

      //réinitialisation threateningPieces: 


      this.evaluateCheckedConfiguration();

      //dispatch une action pour stocker thratenign piece de this.checkeCOnfiguration 

      
      this.store.dispatch(storeThreateningPieces({ threateningPieces: this.threateningPieces }))
     

    });
  }


  /**
  * Returns the possible absolute coordinates the given piece can move to.
  *
  * @param {Piece} piece - The piece to determine possible moves for.
  * @returns {Coords[]} - An array of possible move coordinates.
  */
  showPossibleAbsoluteCoords(piece: Piece): Coords[] {
    let possibleCoords: Array<Coords> = [];

    const { coords, color } = piece;
    const { x, y } = coords;

    possibleCoords = [
      { x: x + 1, y: y },
      { x: x - 1, y: y },

      { x: x, y: y + 1 },
      { x: x, y: y - 1 },

      { x: x + 1, y: y - 1 },
      { x: x + 1, y: y + 1 },

      { x: x - 1, y: y - 1 },
      { x: x - 1, y: y + 1 },
    ];

    const possibleMovesToPiece = this.coordUtilService.coordsToPiecesVariant(possibleCoords);

    //if theres is an opposite piece in the piece array
    const possibleEnnemyPieces = possibleMovesToPiece.filter((p: Piece) => p.color !== piece.color && p.pieceName !== 'empty');
    if (possibleEnnemyPieces.length !== 0) {
      //is this piece covered by ally ?
      const ennemyPieceCovered: Piece[] = [];
      possibleEnnemyPieces.forEach((p: Piece) => {
        if (this.pieceService.isPieceCovered(p)) {
          ennemyPieceCovered.push(p);
        }
      });
      const ennemyCoordsCoveredToRemove: Coords[] = this.coordUtilService.piecesToCoords(ennemyPieceCovered);
      // Remove the covered enemy coordinates from possible moves
      possibleCoords = possibleCoords.filter(coord =>
        !ennemyCoordsCoveredToRemove.some(coveredCoord =>
          coveredCoord.x === coord.x && coveredCoord.y === coord.y
        )
      );

    }

    if (this.isSmallRookPossible(piece)) {
      const smallRookCoords: Coords[] = [
        { x: x + 1, y: y },
        { x: x + 2, y: y },
      ]

      const isSmallRookNotEmpty = this.coordUtilService.coordsToPiecesVariant(smallRookCoords).some((p: Piece) => p.pieceName != 'empty')

      const isRookExposed = this.isRookExposed(smallRookCoords);

      if (!isRookExposed && !isSmallRookNotEmpty) {
        possibleCoords = [...possibleCoords, ...smallRookCoords];


        const tower: Piece = this.pieces.find(
          (p: Piece) => p.pieceName === 'tower' && p.coords.x === (piece.coords.x + 3) && p.color === piece.color) as Piece;
    

        this.store.dispatch(updateSmallRook({smallRookStatus: true, rookableTower: tower}));
      }
    }

    if (this.isGrandRookPossible(piece)) {

      const grandRookCoords: Coords[] = [
        { x: x - 1, y: y },
        { x: x - 2, y: y },
      
      ]

      const isGrandRookNotEmpty = this.coordUtilService.coordsToPiecesVariant([...grandRookCoords, { x: x - 3, y: y }]).some((p: Piece) => p.pieceName != 'empty')

      const isRookExposed = this.isRookExposed(grandRookCoords);

      if (!isRookExposed && !isGrandRookNotEmpty) {
        possibleCoords = [...possibleCoords, ...grandRookCoords];

        const tower: Piece = this.pieces.find(
          (p: Piece) => p.pieceName === 'tower' && p.coords.x === (piece.coords.x - 4 ) && p.color === piece.color) as Piece;
    

        this.store.dispatch(updateGrandRook({grandRookStatus: true, rookableTower: tower}));
      }

    }

    return possibleCoords;
  }

  isRookExposed(rookCoords: Coords[]): boolean {
    return rookCoords.some(rookCoord =>
      this.allPossibleEnnemyMoves.some(ennemyMove =>
        ennemyMove.x === rookCoord.x && ennemyMove.y === rookCoord.y
      )
    );
  }

  isSmallRookPossible(king: Piece): boolean{
    const tower: Piece = this.pieces.find(
      (p: Piece) => p.pieceName === 'tower' && p.coords.x === (king.coords.x + 3) && p.color === king.color) as Piece;

    if(!tower){
      return false;
    }
    return !king.touched && !tower.touched
  }


  isGrandRookPossible(king: Piece): boolean{

    const tower: Piece = this.pieces.find((p: Piece) => p.pieceName === 'tower' && p.coords.x === (king.coords.x - 4) && p.color === king.color) as Piece;

    if(!tower){
      return false;
    }



    return !king.touched && !tower.touched

  }


  /**
 * Determines if the king is checked.
 * 
 * @param {Piece} king - The king piece to check.
 * @returns {boolean} - True if the king is checked, otherwise false.
 */
  isKingChecked(king: Piece): boolean {
    return this.pieceService.isKingChecked(king);
  }

  findNextPlayerKing(piecePlayed: Piece): Piece | null {
    let oppositeColor = piecePlayed.color === 'white' ? 'black' : 'white';
    return piecePlayed
      ? <Piece>(
        this.pieceService.pieces.find(
          (p: Piece) => p.pieceName === 'king' && p.color === oppositeColor
        )
      )
      : null;
  }


  /**
   * Evaluates the current configuration to determine if the king is checked.
   * Checks each direction and possible threatening pieces.
   */
  evaluateCheckedConfiguration(): void {

    this.scanDirection({ x: 1, y: -1 }, ['bishop', 'queen']);
    this.scanDirection({ x: 1, y: 1 }, ['bishop', 'queen']);
    this.scanDirection({ x: -1, y: 1 }, ['bishop', 'queen']);
    this.scanDirection({ x: -1, y: -1 }, ['bishop', 'queen']);
    this.scanDirection({ x: 0, y: -1 }, ['tower', 'queen']);
    this.scanDirection({ x: 0, y: 1 }, ['tower', 'queen']);
    this.scanDirection({ x: -1, y: 0 }, ['tower', 'queen']);
    this.scanDirection({ x: 1, y: 0 }, ['tower', 'queen']);
    this.scanEnnemyKnights();
    this.scanEnnemyPawns();

  }


  /**
   * Scans in a given direction for possible threatening pieces.
   * 
   * @param {Coords} direction - The direction to scan in.
   * @param {string[]} validPieces - The pieces that can be a threat in this direction.
   */
  scanDirection(direction: Coords, validPieces: string[]): void {
    const { x, y } = this.kingChecked!.coords!;
    const maxDistance = 7;

    let foundThreats = 0;  // Counter to track the number of threats

    for (let distance = 1; distance <= maxDistance; distance++) {
      const coord = {
        x: x + direction.x * distance,
        y: y + direction.y * distance
      };
      const pieceAtCoord = this.pieces.find(p => this.pieceService.isSameCoords(coord, p.coords));
      if (!pieceAtCoord || pieceAtCoord.pieceName === 'empty') {
        if (foundThreats === 0) {
          this.checkedConfiguration.emptyCoords.push(coord);
        }
        continue;
      }

      // If it's not a valid threatening piece or of the same color as the king, break
      if (!validPieces.includes(pieceAtCoord.pieceName) || pieceAtCoord.color === this.kingChecked?.color) {
        break;
      }

      this.checkedConfiguration.threateningPieces.push(pieceAtCoord);

      this.threateningPieces = [...this.threateningPieces, pieceAtCoord];
      foundThreats++;

      // If we've already found 2 threats, break out of the loop
      if (foundThreats === 2) {
        break;
      }
    }
  }

  scanEnnemyPawns(): void{
    const { x, y } = this.kingChecked!.coords;

    const threateningPawns: Piece[] = [];

    let possibleBlackEnnemyPawnPostions: Coords[] = this.kingChecked!.color === 'white' ? [

      { x: x + 1, y: y - 1 },
      { x: x - 1, y: y - 1 },

    ] :
      [

        { x: x + 1, y: y + 1 },
        { x: x - 1, y: y + 1 },

      ];



    possibleBlackEnnemyPawnPostions.forEach((c: Coords) => {

      this.pieces.forEach((p: Piece) => {
        if (
          this.pieceService.isSameCoords(c, p.coords) &&
          (p.pieceName === 'pawn') &&
          p.color !== this.kingChecked?.color
        ) {
          threateningPawns.push(p);
          this.threateningPieces = [...this.threateningPieces, p];
        }
      });
    });



  }

  scanEnnemyKnights(): void {

    const { x, y } = this.kingChecked!.coords;

    let threateningKnights: Piece[] = [];

    let possibleEnnemyKnightPostions: Coords[] = [

      { x: x + 1, y: y - 2 },
      { x: x - 1, y: y - 2 },

      { x: x + 2, y: y - 1 },
      { x: x - 2, y: y - 1 },

      { x: x + 2, y: y + 1 },
      { x: x - 2, y: y + 1 },

    ];

    possibleEnnemyKnightPostions.forEach((c: Coords) => {

      this.pieces.forEach((p: Piece) => {
        if (
          this.pieceService.isSameCoords(c, p.coords) &&
          (p.pieceName === 'knight') &&
          p.color !== this.kingChecked?.color
        ) {
          threateningKnights.push(p);
          this.checkedConfiguration.threateningPieces.push(p)
        }
      });
    });
  }

  /**
 * Scans for pawns in positions that threaten the opposing king.
 *
 * @param {Piece} king - The king that may be under threat.
 */
scanForPawnThreats(king: Piece): void {
  // Pawns threaten diagonally, so the direction depends on the pawn's color
  const directions = king.color === 'white' ? [{ x: -1, y: 1 }, { x: 1, y: 1 }] : [{ x: -1, y: -1 }, { x: 1, y: -1 }];

  directions.forEach((direction) => {
    const potentialThreatCoord = {
      x: king.coords.x + direction.x,
      y: king.coords.y + direction.y,
    };

    // Check if the coordinate is within bounds
    if (this.isWithinBoard(potentialThreatCoord)) {
      const pieceAtCoord = this.pieces.find(p => this.pieceService.isSameCoords(p.coords, potentialThreatCoord));
      // If there's a pawn of the opposite color at the coordinate, it's a threat
      if (pieceAtCoord && pieceAtCoord.pieceName === 'pawn' && pieceAtCoord.color !== king.color) {
        this.checkedConfiguration.threateningPieces.push(pieceAtCoord);
      }
    }
  });
}

/**
 * Determines if the given coordinates are within the bounds of the chessboard.
 *
 * @param {Coords} coords - The coordinates to check.
 * @returns {boolean} - True if the coordinates are within bounds, otherwise false.
 */
isWithinBoard(coords: Coords): boolean {
  return coords.x >= 0 && coords.x <= 7 && coords.y >= 0 && coords.y <= 7;
}



  /**
  * Obtains all the coordinates between the threatening piece and the king.
  * 
  * @param {Piece} king - The king's position.
  * @param {Piece} threateningPiece - The threatening piece's position.
  * @returns {Coords[]} - An array of coordinates between the king and the threatening piece.
  */
  getCoordsBetweenCheckedKingAndFirstThreateningPiece(king: Piece, threateningPiece: Piece): Coords[] {
    let coordsBetween: Coords[] = [];
  
    // Determine the direction of movement towards the king
    let directionX = Math.sign(king.coords.x - threateningPiece.coords.x);
    let directionY = Math.sign(king.coords.y - threateningPiece.coords.y);
  
    // Start from the threatening piece's position
    let currentX = threateningPiece.coords.x + directionX;
    let currentY = threateningPiece.coords.y + directionY;
  
    // Continue until reaching the king's position
    while (currentX !== king.coords.x || currentY !== king.coords.y) {
      // Add the current position to the array
      coordsBetween.push({ x: currentX, y: currentY });
  
      // Move one step in the direction of the king
      currentX += directionX;
      currentY += directionY;
  
      // Add a boundary check to prevent potential infinite loop
      if (currentX < 0 || currentX > 7 || currentY < 0 || currentY > 7) {
        throw new Error('Current coordinates went out of bounds, which should not happen.');
      }
    }
  
    return coordsBetween;
  }

  isContainingKing(possibleMove: Coords[], king: Piece): boolean {
    if (!king || !king.coords) {
      //console.error('Invalid king object:', king);
      return false;
    }
    
    return possibleMove.some(m => PossibleMovesService.isSameCoords(m, king.coords));
  }


}
