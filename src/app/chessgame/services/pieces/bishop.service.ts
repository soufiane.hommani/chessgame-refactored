import { Injectable } from '@angular/core';
import { MovablePiece } from '../../interfaces/movable-piece';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { selectChessboard } from '../../interfaces/store/slices/chessboard-slice/selector';
import { ChessgameState } from '../../interfaces/store/interfaces/chessgame.state';
import { PieceService } from '../piece.service';

@Injectable({
  providedIn: 'root'
})
export class BishopService implements MovablePiece{

  
  constructor( private pieceService: PieceService) {}
  
  showPossibleAbsoluteCoords(piece: Piece): Coords[] {

    let possibleCoords: Array<Coords> = [];
    let northWest = []; 
    let northEast = [];

    const { coords, color } = piece;
    const { x, y } = coords;


    northWest = [
      { x: x - 1, y: y - 1 },
      { x: x - 2, y: y - 2 },
      { x: x - 3, y: y - 3 },
      { x: x - 4, y: y - 4 },
      { x: x - 5, y: y - 5 },
      { x: x - 6, y: y - 6 },
      { x: x - 7, y: y - 7 }
    ]

    northEast = [
      { x: x + 1, y: y - 1 },
      { x: x + 2, y: y - 2 },
      { x: x + 3, y: y - 3 },
      { x: x + 4, y: y - 4 },
      { x: x + 5, y: y - 5 },
      { x: x + 6, y: y - 6 },
      { x: x + 7, y: y - 7 }

    ]


    let southWest = [
      { x: x - 1, y: y + 1 },
      { x: x - 2, y: y + 2 },
      { x: x - 3, y: y + 3 },
      { x: x - 4, y: y + 4 },
      { x: x - 5, y: y + 5 },
      { x: x - 6, y: y + 6 },
      { x: x - 7, y: y + 7 }
    ]

    
    let southEast = [
      { x: x + 1, y: y + 1 },
      { x: x + 2, y: y + 2 },
      { x: x + 3, y: y + 3 },
      { x: x + 4, y: y + 4 },
      { x: x + 5, y: y + 5 },
      { x: x + 6, y: y + 6 },
      { x: x + 7, y: y + 7 }
    ]
   
    northWest = this.pieceService.removeBeyondAllyIndex(northWest,  this.pieceService.findAllyIndex(northWest, <string>color));
    northWest = this.pieceService.removeBeyondAllyIndex(northWest,  this.pieceService.findEnnemyIndex(northWest, <string>color));

    northEast = this.pieceService.removeBeyondAllyIndex(northEast,  this.pieceService.findAllyIndex(northEast, <string>color));
    northEast = this.pieceService.removeBeyondAllyIndex(northEast,  this.pieceService.findEnnemyIndex(northEast, <string>color));

    southWest = this.pieceService.removeBeyondAllyIndex(southWest,  this.pieceService.findAllyIndex(southWest, <string>color));
    southWest = this.pieceService.removeBeyondAllyIndex(southWest,  this.pieceService.findEnnemyIndex(southWest, <string>color));

    southEast = this.pieceService.removeBeyondAllyIndex(southEast,  this.pieceService.findAllyIndex(southEast, <string>color));
    southEast = this.pieceService.removeBeyondAllyIndex(southEast,  this.pieceService.findEnnemyIndex(southEast, <string>color));

    return [...northWest, ...northEast, ...southWest, ...southEast];

  }

  showPossibleAbsoluteBeyondEnnemyCoords(piece: Piece): Coords[] {

    let possibleCoords: Array<Coords> = [];
    let northWest = []; 
    let northEast = [];

    const { coords, color } = piece;
    const { x, y } = coords;


    northWest = [
      { x: x - 1, y: y - 1 },
      { x: x - 2, y: y - 2 },
      { x: x - 3, y: y - 3 },
      { x: x - 4, y: y - 4 },
      { x: x - 5, y: y - 5 },
      { x: x - 6, y: y - 6 },
      { x: x - 7, y: y - 7 }
    ]

    northEast = [
      { x: x + 1, y: y - 1 },
      { x: x + 2, y: y - 2 },
      { x: x + 3, y: y - 3 },
      { x: x + 4, y: y - 4 },
      { x: x + 5, y: y - 5 },
      { x: x + 6, y: y - 6 },
      { x: x + 7, y: y - 7 }

    ]


    let southWest = [
      { x: x - 1, y: y + 1 },
      { x: x - 2, y: y + 2 },
      { x: x - 3, y: y + 3 },
      { x: x - 4, y: y + 4 },
      { x: x - 5, y: y + 5 },
      { x: x - 6, y: y + 6 },
      { x: x - 7, y: y + 7 }
    ]

    
    let southEast = [
      { x: x + 1, y: y + 1 },
      { x: x + 2, y: y + 2 },
      { x: x + 3, y: y + 3 },
      { x: x + 4, y: y + 4 },
      { x: x + 5, y: y + 5 },
      { x: x + 6, y: y + 6 },
      { x: x + 7, y: y + 7 }
    ]
   
    northWest = this.pieceService.removeBeyondAllyIndex(northWest,  this.pieceService.findAllyIndex(northWest, <string>color));

    northEast = this.pieceService.removeBeyondAllyIndex(northEast,  this.pieceService.findAllyIndex(northEast, <string>color));

    southWest = this.pieceService.removeBeyondAllyIndex(southWest,  this.pieceService.findAllyIndex(southWest, <string>color));

    southEast = this.pieceService.removeBeyondAllyIndex(southEast,  this.pieceService.findAllyIndex(southEast, <string>color));

    return [...northWest, ...northEast, ...southWest, ...southEast];

  }

}
