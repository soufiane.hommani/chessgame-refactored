import { Injectable, OnInit } from '@angular/core';
import { Coords } from 'src/app/core/interfaces/coords';
import { ChessgameModule } from '../chessgame.module';
import { PawnService } from './pieces/pawn.service';
import { Piece } from 'src/app/core/interfaces/piece';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ChessgameState } from '../interfaces/store/interfaces/chessgame.state';
import { selectChessboard } from '../interfaces/store/slices/chessboard-slice/selector';
import { selectAfterMovePosition, selectAllPossibleMoves, selectBeforeMovePosition, selectCommonCoveringCoords, selectLastPieceClicked, selectPossibleMoves } from '../interfaces/store/slices/possible-moves-slice/selector';
import { KnightService } from './pieces/knight.service';
import { BishopService } from './pieces/bishop.service';
import { TowerService } from './pieces/tower.service';
import { KingService } from './pieces/king.service';
import { loadAllPossibleMoves } from '../interfaces/store/slices/possible-moves-slice/action';
import { isKingChecked } from '../interfaces/store/slices/king-slice/action';
import { selectCheckedKing, selectThreateningPieces } from '../interfaces/store/slices/king-slice/selector';
import cloneDeep from 'lodash/cloneDeep';
import { CoordsUtilService } from './coords-util.service';



@Injectable({
  providedIn: 'root',
})
export class PossibleMovesService {
  

  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  possibleMoves$: Observable<Array<Coords>> = this.store.pipe(
    select(selectPossibleMoves)
  );
  afterMovePosition$: Observable<Coords> = this.store.pipe(
    select(selectAfterMovePosition)
  );

  beforeMovePosition$: Observable<Coords> = this.store.pipe(
    select(selectBeforeMovePosition)
  );

  commonCoveringCoords$: Observable<Coords[] | null> = this.store.pipe(
    select(selectCommonCoveringCoords)
  );
  commonCoveringCoords: Coords[] = [];


  kingChecked$: Observable<Piece> = this.store.pipe(select(selectCheckedKing));
  kingChecked?: Piece;

  threateningPiece$: Observable<Piece[]> = this.store.pipe(select(selectThreateningPieces));
  threateningPiece?: Piece;

  lastPieceClicked$: Observable<Piece> = this.store.pipe(select(selectLastPieceClicked)) as Observable<Piece>;
  lastPieceClicked!: Piece;

  pieces: Array<Piece> = [];
  afterMovePosition!: Coords;
  beforeMovePosition!: Coords;
  
  allPossibleMoves$: Observable<Array<Coords>> = this.store.pipe(select(selectAllPossibleMoves));
  allPossibleMoves: Coords[] = [];

  constructor(
    private pawnService: PawnService,
    private knightService: KnightService,
    private bishopService: BishopService,
    private towerService: TowerService,
    private kingService: KingService,
    public store: Store<ChessgameState>,
    private cu: CoordsUtilService, 
  ) {

    

    this.pieces$.subscribe((p: Piece[]) => {

      this.pieces = p
      this.allPossibleMoves = this.showAllPossibleMoves(this.lastPieceClicked);
      this.store.dispatch(loadAllPossibleMoves({allPossibleMoves: this.allPossibleMoves}));

    });

    this.afterMovePosition$.subscribe((c: Coords) => {
      this.afterMovePosition = c;
    })

    this.beforeMovePosition$.subscribe((c: Coords) => {
      this.beforeMovePosition = c;
    })

    this.threateningPiece$.subscribe((t: Piece[]) => {
      this.threateningPiece = t[0];
    })

    this.lastPieceClicked$.subscribe((p: Piece) => {
      this.lastPieceClicked = p;
      this.allPossibleMoves = this.showAllPossibleMoves(p);
      this.store.dispatch(loadAllPossibleMoves({allPossibleMoves: this.allPossibleMoves}));
    })

    this.allPossibleMoves$.subscribe((c: any) => {
    })

    this.commonCoveringCoords$.subscribe((cc: Coords[] | null) => { this.commonCoveringCoords = cc as Coords[] });

  }

  previousPlayerPieces: Piece[] = []

  showAllPossibleMoves(lastPieceClicked: Piece): Coords[]{

    if(lastPieceClicked?.pieceName !== 'empty'){
      this.previousPlayerPieces = this.pieces.filter((p: Piece) => p.color === lastPieceClicked?.color)
    }

    let allPossibleMoves: Coords[] = []
    this.previousPlayerPieces.forEach((p: Piece) => allPossibleMoves.push(...this.showPossibleMovesNotObservable(p)));
    
    return allPossibleMoves;

  }

  showPossibleMoves(piece: Piece): Observable<Array<Coords>> {

    let possibleCoords: Array<Coords> = [];
    let { pieceName, color, coords } = piece;

    switch (pieceName) {
      case 'pawn':
        possibleCoords = this.pawnService.showPossibleAbsoluteCoords(piece);
        break;
      case 'knight':
        possibleCoords = this.knightService.showPossibleAbsoluteCoords(piece);
        break;
      case 'bishop':
        possibleCoords = this.bishopService.showPossibleAbsoluteCoords(piece);
        break;
      case 'tower':
        possibleCoords = this.towerService.showPossibleAbsoluteCoords(piece);
        break;
      case 'queen':
        possibleCoords = [
          ...this.towerService.showPossibleAbsoluteCoords(piece),
          ...this.bishopService.showPossibleAbsoluteCoords(piece),
        ];
        break;
      case 'king':
        possibleCoords = this.kingService.showPossibleAbsoluteCoords(piece);
        break;
      default:
        possibleCoords = [];
        break;
    }


    possibleCoords = this.removeSameColorPieceCoords(possibleCoords, piece);
    possibleCoords = possibleCoords.filter((c: Coords) => PossibleMovesService.isCoordsValid(c))
    if(this.commonCoveringCoords.length !== 0 && piece.pieceName !== 'king'){
      possibleCoords = this.cu.findCommonCoordinates(possibleCoords, this.commonCoveringCoords);
    }


    return of(possibleCoords);
  }


  showPossibleDefendingMoves(defendingCoords: Coords[]): Observable<Array<Coords>> {

    let possibleCoords: Array<Coords> = [];
    possibleCoords = possibleCoords.filter((c: Coords) => PossibleMovesService.isCoordsValid(c))
    return of(possibleCoords);
    
  }

  
  showPossibleMovesAbsoluteNotObservable(piece: Piece, isForAdverseCoords: boolean = false): Coords[] {

    let possibleCoords: Array<Coords> = [];
    let { pieceName, color, coords } = piece;

    switch (pieceName) {
      case 'pawn':
        possibleCoords = isForAdverseCoords ? this.pawnService.showPossibleDiagonaleAbsoluteCoords(piece) :
          this.pawnService.showPossibleAbsoluteCoords(piece);
        break;
      case 'knight':
        possibleCoords = this.knightService.showPossibleAbsoluteCoords(piece);
        break;
      case 'bishop':
        possibleCoords = this.bishopService.showPossibleAbsoluteBeyondEnnemyCoords(piece);
        break;
      case 'tower':
        possibleCoords = this.towerService.showPossibleAbsoluteBeyondEnnemyCoords(piece);
        break;
      case 'queen':
        possibleCoords = [
          ...this.towerService.showPossibleAbsoluteBeyondEnnemyCoords(piece),
          ...this.bishopService.showPossibleAbsoluteBeyondEnnemyCoords(piece),
        ];
        break;
      case 'king':
        possibleCoords = this.kingService.showPossibleAbsoluteCoords(piece);
        break;
      default:
        possibleCoords = [];
        break;
    }

    possibleCoords = possibleCoords.filter((c: Coords) => PossibleMovesService.isCoordsValid(c))

    //console.log('possibleCoords here absolute', possibleCoords)

    return possibleCoords;
  }

  showPossibleMovesNotObservable(piece: Piece, isForAdverseCoords: boolean = false): Coords[] {

    let possibleCoords: Array<Coords> = [];
    let { pieceName, color, coords } = piece;

    switch (pieceName) {
      case 'pawn':
        possibleCoords = isForAdverseCoords ? this.pawnService.showPossibleDiagonaleAbsoluteCoords(piece) :
          this.pawnService.showPossibleAbsoluteCoords(piece);
        break;
      case 'knight':
        possibleCoords = this.knightService.showPossibleAbsoluteCoords(piece);
        break;
      case 'bishop':
        possibleCoords = this.bishopService.showPossibleAbsoluteCoords(piece);
        break;
      case 'tower':
        possibleCoords = this.towerService.showPossibleAbsoluteCoords(piece);
        break;
      case 'queen':
        possibleCoords = [
          ...this.towerService.showPossibleAbsoluteCoords(piece),
          ...this.bishopService.showPossibleAbsoluteCoords(piece),
        ];
        break;
      case 'king':
        possibleCoords = this.kingService.showPossibleAbsoluteCoords(piece);
        break;
      default:
        possibleCoords = [];
        break;
    }

    possibleCoords = this.removeSameColorPieceCoords(possibleCoords, piece);
    possibleCoords = possibleCoords.filter((c: Coords) => PossibleMovesService.isCoordsValid(c))

    return possibleCoords;
  }

  showAllPossibleEnnemyMoves(piece: Piece): Coords[] {
    // Determine the enemy's color based on the provided piece's color.
    const enemyColor = piece.color === 'white' ? 'black' : 'white';

    //console.log('enemyColor', enemyColor)
  
    // Filter the pieces to get only the enemy pieces.

    const enemyPieces =  this.pieces.filter(p => p.color === enemyColor);
  
    // Get all possible moves for all enemy pieces.
    let allEnemyPossibleMoves: Coords[] = [];


    enemyPieces.forEach(enemyPiece => {

      allEnemyPossibleMoves = [
        ...allEnemyPossibleMoves,
        ...this.showPossibleMovesNotObservable(enemyPiece, true)
      ];


      allEnemyPossibleMoves = this.removeDuplicateCoords(allEnemyPossibleMoves);



    });
  
    // Return the combined list of all enemy possible moves.
    return allEnemyPossibleMoves;
  }

  removeDuplicateCoords(coordsArray: Coords[]): Coords[] {
    return coordsArray.filter((coord, index, selfArray) => {
      return index === selfArray.findIndex((c) => PossibleMovesService.isSameCoords(c, coord));
    });
  }

  static isSameCoords(c1: Coords, c2: Coords): boolean {
    if (!c1 || !c2) {
      return false;
    }
    return c1.x == c2.x && c1.y == c2.y;
  }

  static isCoordsValid(c1: Coords): boolean {
    return !(c1.x > 7 || c1.y > 7 || c1.x < 0 || c1.y < 0);
  }

  static findTargetIndexInPieces(pieces: Piece[], targetPiece: Piece): number {
    return pieces.findIndex((p: Piece) => this.isSameCoords(p.coords, (targetPiece).coords));
  }

  static findLeavingIndexInPieces(pieces: Piece[], leavingPiece: Piece): number {
    return pieces.findIndex((p: Piece) => PossibleMovesService.isSameCoords(p.coords, leavingPiece.coords));
  }

  isAPiece(pos: Coords): boolean {
    return this.pieces.some((p: Piece) => PossibleMovesService.isSameCoords(p.coords, pos) && p.pieceName != 'empty')
  }


  isPossibleMove(pieceClicked: Piece, possibleMoves: Coords[]): boolean {

    if (possibleMoves.length == 0) {
      return false;
    }

    return possibleMoves.some((p: Coords) =>
      PossibleMovesService.isSameCoords(p, pieceClicked.coords)
    );
  }

  isBeforeOrLastMove(pos: Coords): boolean {
    return PossibleMovesService.isSameCoords(pos, this.afterMovePosition) ||
      PossibleMovesService.isSameCoords(pos, this.beforeMovePosition);
  }


  removeSameColorPieceCoords(possibleCoords: Coords[], pieceClicked: Piece): Coords[] {

    this.pieces.forEach((p: Piece) => {
      possibleCoords = possibleCoords.filter((c: Coords) => !(PossibleMovesService.isSameCoords(c, p.coords) && p.color === pieceClicked.color))
    })

    return possibleCoords;
  }

  getCommonCoords(pieceCoords: Coords[], emptyCoordsKingVSThreat: Coords[]): Coords[] {
    return pieceCoords.filter(pieceCoord => 
        emptyCoordsKingVSThreat.some(emptyCoord => 
            PossibleMovesService.isSameCoords(pieceCoord, emptyCoord)
        )
    );
}


replacePieceNameByEmpty(p: Piece){
  return { ...p, pieceName: 'empty' };
}

clonePieceDeep(piece: Piece): Piece {
  return cloneDeep(piece);
}

clonePiecesDeep(pieces: Piece[]): Piece[] {
  return cloneDeep(pieces);
}

updateCurrentPieces(p: Piece): Piece[] {

  let currentPieces = this.clonePiecesDeep(this.pieces)
  const index = currentPieces.findIndex(piece => PossibleMovesService.isSameCoords(piece.coords, p.coords));
  // If found, replace that piece with p
  if (index !== -1) {
    currentPieces[index] = p;
  }

  return currentPieces;
}

canMoveWithoutLeavingKingUncovered(pieceSelected: Piece): boolean{
  //cloner piece...
  let clonedPiece = this.clonePieceDeep(pieceSelected);
  clonedPiece = this.replacePieceNameByEmpty(clonedPiece);


  return true;
}





}
