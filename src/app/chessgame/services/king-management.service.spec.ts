import { TestBed } from '@angular/core/testing';

import { KingManagementService } from './king-management.service';

describe('KingManagementService', () => {
  let service: KingManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KingManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
