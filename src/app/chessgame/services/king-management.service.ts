import { Injectable } from '@angular/core';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { CoordsUtilService } from './coords-util.service';
import { Store } from '@ngrx/store';
import { ChessgameState } from '../interfaces/store/interfaces/chessgame.state';
import { loadCommonCoveringCoords, loadPossibleMovesWhenCoveringKing } from '../interfaces/store/slices/possible-moves-slice/action';

@Injectable()
export class KingManagementService {

  constructor(private cu: CoordsUtilService, private store: Store<ChessgameState>
  ) { }

  isCoveringKing(piece: Piece, piecePossibleMoves: Coords[]): boolean {

    // Get all line and diagonal pieces that could potentially cover the king
    const lineAndDiagonalPieces = [
      ...this.getDiagonalPieces(piece, '/'),
      ...this.getDiagonalPieces(piece, '\\'),
      ...this.getLinePieces(piece, 'vertical'),
      ...this.getLinePieces(piece, 'horizontal')
    ];

    // console.log('lineAndDiagonalPieces original', lineAndDiagonalPieces)


    const threats = this.findThreats(piece, lineAndDiagonalPieces);
    const firstThreat: Piece | null = threats ? threats[threats.length - 1] : null

    // console.log('threats', threats)
    // console.log('firstThreat', firstThreat)
    // console.log('piece clicked', piece)

    const pieceClickedAllyKingSpace: Piece[] = this.cu.calculatePieceClickedAllyKingSpace(lineAndDiagonalPieces, piece);
    const pieceClickedFirstThreatSpace: Piece[] = this.cu.calculatePieceClickedFirstThreatSpace(lineAndDiagonalPieces, piece, firstThreat);

    // console.log('possibleMoves pice cliquée avant common', piecePossibleMoves)
     //console.log('pieceClickedFirstThreatSpace', pieceClickedFirstThreatSpace)


    if (firstThreat) {
      pieceClickedFirstThreatSpace.push(firstThreat);
    }

    let commonCoords: Coords[] = this.cu.findCommonCoords(pieceClickedFirstThreatSpace, piecePossibleMoves)

    
    /*        8  . . . . . . . .
              7  . . . bQ . . . .
              6  . . . wQ . . . .
              5  . . . X . . . . 
              4  . . . X . . . .
              3  . . . X . . . .
              2  . . . X . . . .
              1  . . . wK . . . .
                 a b c d e f g h
              X: should be reachable
    */
    if (commonCoords.length > 0) {
      commonCoords.push(...pieceClickedAllyKingSpace.map(p => p.coords));
    }

    // Check for threats in those pieces

    // console.log('lineAndDiagonalPieces', lineAndDiagonalPieces)
    // console.log('pieceClickedAllyKingSpace', pieceClickedAllyKingSpace)
    // console.log('pieceClickedFirstThreatSpace', pieceClickedFirstThreatSpace)
    // console.log('commonCoords', commonCoords)

    //if a non empty piece stands between piececliked and first threat
    //or if a non empty piece stands between piececliked and ally king
    // => piece can move
    //console.log('pieceClickedFirstThreatSpace.some((p: Piece) => p.pieceName !== empty)', pieceClickedFirstThreatSpace.some((p: Piece) => p.pieceName !== 'empty'))
    if (pieceClickedAllyKingSpace.some((p: Piece) => p.pieceName !== 'empty' && p.color === piece.color)
      || pieceClickedFirstThreatSpace.some((p: Piece) => p.pieceName !== 'empty' && p.color === piece.color) && pieceClickedFirstThreatSpace.length !== 0) {
      //this.store.dispatch(loadCommonCoveringCoords({ commonCoveringCoords: commonCoords }));
      // console.log('here 1')
      return false
    }

    //allow piece movement if its possible move is within pieceClickedFirstThreatSpace
    //I need its possible move first

    if (commonCoords.length !== 0) {
      //dispatch une action pour remplir possible coords à commonCoords
      // console.log('passe par la jojgoze^pro ', commonCoords)
      this.store.dispatch(loadCommonCoveringCoords({ commonCoveringCoords: commonCoords }));
      return false

      //créer une propriété dans la slice possibleMoves coveringCommonCoords et filtrer dessus dans le service possible moves

    } else if (commonCoords.length === 0 && pieceClickedFirstThreatSpace.length !== 0) {
      // console.log('passe par la forcement')
      return true
    }






    // Return true if there are any threats, otherwise false
    return threats.length > 0;
  }

  private findThreats(piece: Piece, pieces: Piece[]): Piece[] {
    const enemyColor = piece.color === 'white' ? 'black' : 'white';
    const kingCoords = this.cu.findKingCoords(piece.color as 'white' | 'black');
    const threats: Piece[] = [];

    for (const p of pieces) {
      if (this.isThreat(piece, p, kingCoords!, enemyColor)) {
        threats.push(p);
      }
    }

    return threats;
  }

  private isThreat(allyPiece: Piece, enemyPiece: Piece, kingCoords: Coords, enemyColor: string): boolean {
    if (enemyPiece.color !== enemyColor) return false;

    const isDiagonal = this.cu.isDiagonal(kingCoords, enemyPiece.coords);
    const isLine = this.cu.isSameLine(kingCoords, enemyPiece.coords);
    const isQueen = enemyPiece.pieceName === 'queen';
    const isRook = enemyPiece.pieceName === 'tower' && isLine;
    const isBishop = enemyPiece.pieceName === 'bishop' && isDiagonal;

    return (isQueen || isRook || isBishop) && this.cu.isPathClear(kingCoords, enemyPiece.coords);
  }

  private getDiagonalPieces(piece: Piece, direction: '/' | '\\'): Piece[] {
    const diagonalCoords = this.calculateDiagonalCoords(piece, direction);
    return this.cu.coordsToPieces(diagonalCoords, piece);
  }

  private calculateDiagonalCoords(piece: Piece, direction: '/' | '\\'): Coords[] {
    const coords = this.cu.calculateDiagonalCoords(piece.coords, direction);
    return this.cu.removeOutOfBoundsCoords(coords);
  }

  private getLinePieces(piece: Piece, direction: 'vertical' | 'horizontal'): Piece[] {
    const lineCoords = this.calculateLineCoords(piece, direction);
    return this.cu.coordsToPieces(lineCoords, piece);
  }

  private calculateLineCoords(piece: Piece, direction: 'vertical' | 'horizontal'): Coords[] {
    const coords = this.cu.calculateLineCoords(piece.coords, direction);
    return this.cu.removeOutOfBoundsCoords(coords);
  }

}


