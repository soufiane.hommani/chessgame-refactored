import { TestBed } from '@angular/core/testing';

import { CoordsUtilService } from './coords-util.service';

describe('CoordsUtilService', () => {
  let service: CoordsUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoordsUtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
