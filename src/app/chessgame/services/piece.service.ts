import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Coords } from 'src/app/core/interfaces/coords';
import { Piece } from 'src/app/core/interfaces/piece';
import { ChessgameState } from '../interfaces/store/interfaces/chessgame.state';
import { Observable } from 'rxjs';
import { selectChessboard } from '../interfaces/store/slices/chessboard-slice/selector';
import { selectAllPossibleMoves } from '../interfaces/store/slices/possible-moves-slice/selector';
import { CoordsUtilService } from './coords-util.service';
import { updatePieceCoveredByAlly } from '../interfaces/store/slices/chessboard-slice/action';
import { updateFirstThreat } from '../interfaces/store/slices/king-slice/action';

@Injectable({
  providedIn: 'root'
})
export class PieceService {


  pieces$: Observable<Array<Piece>> = this.store.pipe(select(selectChessboard));
  pieces: Array<Piece> = [];

  allPossibleMoves$: Observable<Array<Coords>> = this.store.pipe(select(selectAllPossibleMoves));
  allPossibleMoves: Array<Coords> = [];


  constructor(private store: Store<ChessgameState>, private coordsUtilService: CoordsUtilService) {

    this.pieces$.subscribe((p: Piece[]) => {
      this.pieces = p
    });

    this.allPossibleMoves$.subscribe((c: Coords[]) => {
      this.allPossibleMoves = c;
    });


  }

  isKingChecked(king: Piece): boolean {
    return this.allPossibleMoves.some((c: Coords) => this.isSameCoords(king.coords, c));
  }

  isSameCoords(c1: Coords, c2: Coords): boolean {
    if (!c1 || !c2) {
      return false;
    }
    return c1.x == c2.x && c1.y == c2.y;
  }


  findAllyIndex(directionCoords: Coords[], pieceColor: string): number {

    const indiceCorrespondant = directionCoords.findIndex((coords: Coords) => {
      return this.pieces.some(piece => {
        return piece.coords.x === coords.x && piece.coords.y === coords.y && piece.color === pieceColor;
      });
    });

    return indiceCorrespondant;
  }

  findEnnemyIndex(directionCoords: Coords[], pieceColor: string): number {

    const indiceCorrespondant = directionCoords.findIndex((coords: Coords) => {
      return this.pieces.some(piece => {
        return piece.coords.x === coords.x && piece.coords.y === coords.y && piece.color !== pieceColor && piece.pieceName !== 'empty';
      });
    });

    return indiceCorrespondant;
  }

  removeBeyondAllyIndex(directionCoords: Coords[], allyIndex: number) {
    return allyIndex >= 0 && allyIndex < directionCoords.length ? directionCoords.slice(0, allyIndex + 1) : directionCoords;

  }

  /**
 * Checks if two given pieces are the same
 * @param piece1 First piece to compare
 * @param piece2 Second piece to compare
 * @returns Boolean indicating whether the pieces are equal
 */
  isSamePiece(piece1: Piece, piece2: Piece): boolean {
    return (
      piece1.pieceName === piece2.pieceName &&
      piece1.color === piece2.color &&
      piece1.coords.x === piece2.coords.x &&
      piece1.coords.y === piece2.coords.y &&
      piece1.selected === piece2.selected
    // add any other properties you want to compare, if needed
  );

  }

  isPieceCovered(ennemyPiece: Piece): boolean{

    const isThreatCovered: boolean = this.isCoveredByKing(ennemyPiece)
    || this.isCoveredByPawn(ennemyPiece)
    || this.isCoveredByKnight(ennemyPiece)
    || this.isCoveredByQueen(ennemyPiece)
    || this.isCoveredByBishop(ennemyPiece)
    || this.isCoveredByTower(ennemyPiece);

    //console.log(ennemyPiece, 'piece covered', isThreatCovered)

    return isThreatCovered;
    
  }

  
  isFirsThreatCovered(firstThreat: Piece): void{

    const resetIsCoveredFirstThreat = {
      ...firstThreat,
      isCovered: false,
    };

    this.store.dispatch(updatePieceCoveredByAlly({updatedPiece: resetIsCoveredFirstThreat}));

    // console.log('firstThreat reset', firstThreat)
    // console.log('isCoveredByPawn', this.isCoveredByPawn(firstThreat))
    // console.log('isCoveredByKnight', this.isCoveredByKnight(firstThreat))
    // console.log('isCoveredByQueen', this.isCoveredByQueen(firstThreat))
    // console.log('isCoveredByBishop', this.isCoveredByBishop(firstThreat))
    // console.log('isCoveredByTower', this.isCoveredByTower(firstThreat))

    const isThreatCovered: boolean = this.isCoveredByKing(firstThreat)
      || this.isCoveredByPawn(firstThreat)
      || this.isCoveredByKnight(firstThreat)
      || this.isCoveredByQueen(firstThreat)
      || this.isCoveredByBishop(firstThreat)
      || this.isCoveredByTower(firstThreat);


     // console.log('this.isCoveredByBishop(firstThreat)', this.isCoveredByBishop(firstThreat))
      //console.log('this.isCoveredByQueen(firstThreat)', this.isCoveredByQueen(firstThreat))
      //console.log('isThreatCovered', isThreatCovered)

    const updatedFirstThreat = {
      ...firstThreat,
      isCovered: isThreatCovered,
    };

    //console.log('updatedFirstThreat', updatedFirstThreat)

    this.store.dispatch(updatePieceCoveredByAlly({updatedPiece: updatedFirstThreat}));
    this.store.dispatch(updateFirstThreat({threateningPiece: updatedFirstThreat}));


   // const updateThreatIndex = this.threateningPieces.findIndex(piece => this.pieceService.isSamePiece(piece, updatedPiece))

    // console.log('updatedFirstThreat is covered ??', updatedFirstThreat);
    // console.log('isThreatCovered', isThreatCovered);


  }

  isCoveredByTower(firstThreat: Piece): boolean {
    const { x, y } = firstThreat.coords;
  
    // Define the arrays for each line direction
    const lines: { [key: string]: Coords[] } = {
      topVertical: [],
      bottomVertical: [],
      leftHorizontal: [],
      rightHorizontal: []
    };
  
     // Populate the line arrays with the coordinates, starting from the threat's position
  for (let i = 1; i < 8; i++) {
    // Vertical lines
    if (y - i >= 0) lines.topVertical.push({ x: x, y: y - i }); // Top
    if (y + i < 8) lines.bottomVertical.push({ x: x, y: y + i }); // Bottom
    
    // Horizontal lines
    if (x - i >= 0) lines.leftHorizontal.push({ x: x - i, y: y }); // Left
    if (x + i < 8) lines.rightHorizontal.push({ x: x + i, y: y }); // Right
  }
  
    // Convert the coordinate arrays to arrays of pieces
    const linesToPieces = this.coordsUtilService.linesToPieces(lines);
    //console.log('linesToPieces', linesToPieces)
    const isUnObstructed = this.coordsUtilService.isTowerUnobstructedAfterEmpty(linesToPieces, firstThreat.color as string);
    // Use a helper function to check if there's an unobstructed rook/tower in any line
    //console.log('isUnObstructed', isUnObstructed)
    return isUnObstructed;
  }
  
  isCoveredByBishop(firstThreat: Piece): boolean {

    //console.log("firstThreat", firstThreat)
    const { x, y } = firstThreat.coords;
    const bishopFirstThreatRange: Coords[] = [];

    const diagonales: { [key: string]: Coords[] } = {
      topLeft: [],
      topRight: [],
      bottomLeft: [],
      bottomRight: []
    };

    // Calculate diagonal ranges for the bishop
    for (let i = 1; i < 8; i++) {
      // Diagonal to the top-right
      if (x + i < 8 && y + i < 8) diagonales.topRight.push({ x: x + i, y: y + i });
      // Diagonal to the top-left
      if (x - i >= 0 && y + i < 8) diagonales.topLeft.push({ x: x - i, y: y + i });
      // Diagonal to the bottom-right
      if (x + i < 8 && y - i >= 0) diagonales.bottomRight.push({ x: x + i, y: y - i });
      // Diagonal to the bottom-left
      if (x - i >= 0 && y - i >= 0) diagonales.bottomLeft.push({ x: x - i, y: y - i });
    }

    //console.log('diagonales', diagonales)
    const diagonalesToPieces = this.coordsUtilService.diagonalesToPieces(diagonales);
    //console.log('diagonalesToPieces', diagonalesToPieces)
    const isUnObstructed = this.coordsUtilService.isBishopUnobstructedAfterEmpty(diagonalesToPieces, firstThreat.color as string);
    //console.log('isUnObstructed ', isUnObstructed);

    return isUnObstructed;

  }
  

  
  isCoveredByQueen(firstThreat: Piece): boolean {


    //console.log('firstThreat', firstThreat)

    const { x, y } = firstThreat.coords;
  
    const linesAndDiagonales: { [key: string]: Coords[] } = {
      topLeft: [],
      topRight: [],
      bottomLeft: [],
      bottomRight: [],
      topVertical: [],
      bottomVertical: [],
      leftHorizontal: [],
      rightHorizontal: []
    };
  
    // Populate the arrays with the coordinates, starting from the threat's position
    for (let i = 1; i < 8; i++) {
      // Vertical lines
      if (y - i >= 0) linesAndDiagonales.topVertical.push({ x: x, y: y - i });
      if (y + i < 8) linesAndDiagonales.bottomVertical.push({ x: x, y: y + i });
  
      // Horizontal lines
      if (x - i >= 0) linesAndDiagonales.leftHorizontal.push({ x: x - i, y: y });
      if (x + i < 8) linesAndDiagonales.rightHorizontal.push({ x: x + i, y: y });
  
      // Diagonals
      
      //ok
      if (x + i < 8 && y + i < 8) linesAndDiagonales.bottomRight.push({ x: x + i, y: y + i });
      if (x - i >= 0 && y + i < 8) linesAndDiagonales.bottomLeft.push({ x: x - i, y: y + i });
      if (x + i < 8 && y - i >= 0) linesAndDiagonales.topRight.push({ x: x + i, y: y - i });
      if (x - i >= 0 && y - i >= 0) linesAndDiagonales.topLeft.push({ x: x - i, y: y - i });
    }
  
    // Convert the coordinate arrays to arrays of pieces
      const linesAndDiagonalesToPieces = this.coordsUtilService.linesAndDiagonalesToPieces(linesAndDiagonales);

      //console.log('linesAndDiagonalesToPieces', linesAndDiagonalesToPieces)
  
    // Check if there's an unobstructed queen in any line
    const isUnObstructedLineAndDiagonales = this.coordsUtilService.isKingThreatCovered(linesAndDiagonalesToPieces, firstThreat.color as string);
  
  
    //console.log('isUnObstructedLineAndDiagonales', isUnObstructedLineAndDiagonales)
    // The queen is covered if it's unobstructed in any line or diagonal
    return isUnObstructedLineAndDiagonales;
  }
  


  

  isCoveredByKnight(firstThreat: Piece): boolean{

    const {x, y} = firstThreat.coords;
    const color = firstThreat.color;

    const knightFirstThreatRange: Coords[] = [

      { x: x + 2, y: y + 1 },
      { x: x + 2, y: y - 1 },

      { x: x - 2, y: y - 1 },
      { x: x - 2, y: y + 1 },

      { x: x - 1, y: y + 2 },
      { x: x + 1, y: y + 2 },

      { x: x - 1, y: y - 2 },
      { x: x + 1, y: y - 2 }

    ]

    return this.coordsUtilService.coordsToPiecesContainsAllyCoveringPiece(knightFirstThreatRange, firstThreat, 'knight');

  }

  isCoveredByKing(firstThreat: Piece): boolean{

    const {x, y} = firstThreat.coords;

    const kingFirstThreatRange = [

      {x: x - 1, y: y - 1},
      {x: x, y: y - 1},
      {x: x + 1, y: y - 1},
      {x: x + 1, y: y},
      {x: x - 1, y: y},
      {x: x - 1, y: y + 1},
      {x: x, y: y + 1},
      {x: x + 1, y: y + 1}

    ]

    return this.coordsUtilService.coordsToPiecesContainsAllyKing(kingFirstThreatRange, firstThreat);

  }

  isCoveredByPawn(firstThreat: Piece){

    const {x, y} = firstThreat.coords;
    const color = firstThreat.color;

    let pawnFirstThreatRange: Coords[] = []

    if(color === 'white'){

      pawnFirstThreatRange = [
        {x: x - 1, y: y + 1},
        {x: x + 1, y: y + 1}
      ]

    }else{

      pawnFirstThreatRange = [
        {x: x - 1, y: y - 1},
        {x: x + 1, y: y - 1}
      ]

    }
    
    return this.coordsUtilService.coordsToPiecesContainsAllyCoveringPiece(pawnFirstThreatRange, firstThreat, 'pawn');

  }





}
