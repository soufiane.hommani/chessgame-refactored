import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChessgameModule } from './chessgame/chessgame.module';

const routes: Routes = [

  {
    path: '',
    redirectTo: '/chessboard',
    pathMatch: 'full'
  },


  {
    path: 'chessboard',
    loadChildren: () => import('./chessgame/chessgame.module').then(m => m.ChessgameModule)
  },

  //redirects unknown routes to a known one
  { path: '**', redirectTo: 'chessboard'}



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
