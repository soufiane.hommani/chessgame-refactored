import { TestBed } from '@angular/core/testing';

import { ChessboardSettingsService } from './chessboard-settings.service';

describe('ChessboardSettingsService', () => {
  let service: ChessboardSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChessboardSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
