import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Coords } from '../interfaces/coords';

export interface chessBoardTheme {
  name: string, black: string, white: string, current: string, beforeAfterPosition: string
}

@Injectable({
  providedIn: 'root',
})
export class ChessboardSettingsService {
  /**
   * @param chessboardOrientation : true if player's color is white,
   * false if black.
   */
  private chessboardOrientation: boolean = true;
  
  themes = [
    { name: 'brown', black: '#F0D9B5', white: '#946f51', current: 'rgba(58,112,58,0.8157387955182073)', beforeAfterPosition: 'radial-gradient( rgba(235,245,236,1) 0%, rgba(171,231,11,0.24711134453781514) 0%)' },
    { name: 'blue', black: '#8ca2ad', white: '#dee3e6', current: 'radial-gradient(rgba(235,245,236,1) 0%, rgba(66,255,27,0.24711134453781514) 0%)', beforeAfterPosition: 'radial-gradient(rgba(235,245,236,1) 0%, rgba(66,255,27,0.24711134453781514) 0%)' },
    { name: 'light-green', black: '#86a666', white: '#ffffdd', current: 'radial-gradient(rgba(235,245,236,1) 0%, rgba(27,255,171,0.24711134453781514) 0%)', beforeAfterPosition: 'radial-gradient(rgba(235,245,236,1) 0%, rgba(27,255,171,0.24711134453781514) 0%)' },

  ];
  
  currentTheme$: BehaviorSubject<chessBoardTheme> = new BehaviorSubject<chessBoardTheme>(this.themes[0]);

  constructor() {
    // this.currentTheme = this.themes[0];
  }

  getCbOrientation(): boolean {
    return this.chessboardOrientation;
  }

  setCbOrientation(orientation: boolean): void {
    this.chessboardOrientation = orientation;
  }

  changeChessboardTheme(theme: string): void {
    const newTheme = this.themes.find((t: any) => `${t.name}` === theme);
    this.currentTheme$.next(newTheme!);
  }


  /**
   * @method coordsToCssTopAndLeftPositions - convert xy coord position to their
   * css top & left collaterals
   * @param pieceCoords
   * @returns string of css top and left property
   */
   coordsToCssTopAndLeftPositions(pieceCoords: Coords, orientation: boolean): string {
    if(orientation)
      return `left:${pieceCoords.x *  12.57}% ; top: ${pieceCoords.y *  12.5}%;`;
    else
      return `left:${(7 - pieceCoords.x)* 12.57}% ; top: ${(7 - pieceCoords.y) * 12.5}%;`;
      
  }

}
