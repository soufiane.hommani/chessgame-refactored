/**
 * @interface Piece is responsible for expressing piece properties
 */

import { Coords } from "./coords";

 export interface Piece {
   pieceName: string;
   color: string | boolean;
   coords: Coords;
   selected: boolean;
   imagePath: string;
   isCovered: boolean;
   touched?: boolean;
   canMove?: boolean;
   isPrenable?: boolean;
 }

export const MOCK_PIECE: Piece = { "pieceName": "",
 "color": "white", "coords": { "x": -1, "y": -1 },
  "selected": false, "imagePath": "",
  "isCovered": false,
   "touched": false, "canMove": true, "isPrenable": false };


 