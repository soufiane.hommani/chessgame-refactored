import { LastPieceClickedState } from "src/app/chessgame/interfaces/store/interfaces/last-piece-clicked";
import { TurnToPlayState } from "../../../chessgame/interfaces/store/interfaces/turn-to-play-state";

export interface AppStateInterface {

   turnToPlay: TurnToPlayState;
   lastPieceClicked: LastPieceClickedState;

}