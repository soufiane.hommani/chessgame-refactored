/**
 * @interface Coords is responsible for expressing piece coordinates on a board
 */

 export interface Coords {
    x: number;
    y: number;
}
